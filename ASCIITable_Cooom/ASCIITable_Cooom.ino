

int VID[3]={0x01,0x02,0x03};    //Vendor ID
int PID[3]={0x01,0x02,0x03};    //Product ID
int contacts[256];

void arraySend(int *array, int size)
{
  int *ptr=&array[0];
  while(size)
  {
      Serial.write(*ptr);
      ptr++;
      size--;
  }
  
}



void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    asm("nop");
  }

  // prints title with ending line break
  //Serial.println(sizeof(VID)/sizeof(VID[0]));
  arraySend(VID,3);
  Serial.println();
  Serial.println("ASCII Table ~ Character Map");
}


int thisByte = 33;


void loop() {
  
  Serial.write(thisByte);

  
  Serial.print(", hex: ");
  // prints value as string in hexadecimal (base 16):
  Serial.print(thisByte, HEX);

    Serial.print(", bin: ");
  // prints value as string in binary (base 2)
  // also prints ending line break:
  Serial.println(thisByte, BIN);

  // if printed last visible character '~' or 126, stop:
  if (thisByte == 126)
  // thisByte=33;
  Serial.write(0x04);   
  else thisByte++;
  delay(500);
}
