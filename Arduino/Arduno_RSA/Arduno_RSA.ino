class RSA
	{
		
	// sizeof INT: 2 bytes
	private:
		int prime1,
			prime2,
			phi,
			d=3;//privateKey();	

	public:	
		int n,
			e=7;

		//constructor 
		RSA(){}
		//constructor cu cap
		RSA(int prime1, int prime2, int encryptKey)
		{
			this->prime1=prime1;
			this->prime2=prime2;
			this->n=prime1*prime2;
			this->phi=(prime1-1)*(prime2-1);
		}
		

		int rsaCrypt(int msg)
		{
			return crypt(msg,e,n);
		}


		//mesaj^putere mod modul
		static int crypt(int msg,int exp, int modul )
	    {
	        unsigned int iter=0;
	        unsigned long c=1;
	        while (iter<exp)
	        {
	            c = c*msg;
	            c = c%modul;
	            iter++;
	            asm("NOP");
	        }
	        return c;
	    }

		
		// Print information object
		void printInfo(void)
		{	
			Serial.print("\n rsa Prime number 1=  ");Serial.println(prime1);
			Serial.print(" rsa Prime number 2=  ");Serial.println(prime2);
			Serial.print(" rsa n=  ");Serial.println(n);
			Serial.print(" rsa d=  ");Serial.println(d);
			Serial.print(" rsa e=  ");Serial.println(e);

		}
		int privateKey()
		{
			int D, res = 0;
				for (D = 1;; D++)
				{
					res = (D*e) % phi;
					if (res == 1) break;
				}
				return D;
		}


	};

	

// public: static int PrivateKey(int phi, int E, int N)
// 	{
// 				int D, res = 0;
// 				for (D = 1;; D++)
// 				{
// 					res = (D*E) % phi;
// 					if (res == 1) break;
// 				}
// 				return D;

// 	}

//
void (*reset)(void)=0; //program reset chip

/*
sizeof INT:    		2 bytes
sizeof FLOAT:  		4 bytes
sizeof DOUBLE: 		4 bytes
sizeof uLOND:  		4 bytes
sizeof LOND:   		4 bytes
sizeof WORD:   		2 bytes
sizeof long long 	8 bytes
*/

void setup() {
	bool ok=0;
 	int p1=61,	//prime 1
 		p2=133,	// prime 2
 		e=1657,  //pulbic key 41,13
 		d=73,	//private  key
 		n=p1*p2,  // modulus
 		phi=(p1-1)*(p2-1);

 		RSA r1(61,133,1657);
 		delay();
 		
  // initialize both serial ports:
  Serial.begin(9600);
  delay(200);
  Serial.println("\tArduino MEGA 2560:\n ");

  Serial.print("random number ");Serial.println(random(256)); 

  Serial.print("\nIntervalul de cryptare 0..."); Serial.println(n); 
  
  r1.printInfo();
  

  Serial.println();

 

  //Serial1.begin(9600);
  
  Serial.print(" Public Key{ ");	Serial.print(e);
  Serial.print(", "); Serial.print(n); Serial.println("}");
  Serial.print(" Privat Key{ ");	Serial.print(d);
  Serial.print(", "); Serial.print(n); Serial.println("}\n");


 int a;
		for (int i=32; i<=40; i++)
		{
				 //564
				Serial.print("\norig= "); Serial.print((char)i);
				a=RSA::crypt(i,e,n);
				Serial.print(" --rsaCrypt: "); Serial.print(a); 				
				delay(20);
				//564 564,61,689
				Serial.print(" --rsaDrypt: "); 
				a=RSA::crypt(a,d,n);
				Serial.println((char)a); 
				if (a==i) ok=true;
			}

			

			if (ok) 
				Serial.print("\nEncrypt OK ");
			 else 
			 	Serial.print("\nEncrypt Fail ");

			Serial.print("\nEncrypt n-1: ");Serial.println(RSA::crypt(n-1,e,n));  
			 



/* 	lcd 65w, 
	cpu 140W,
	GPU 170..500W,
	mother  230W
	nominal Power consum: 635 W 
	*/
// buf=0;
// int i=0;
// // statement
// 	delay(300);
	
// 	buf=buf+1;
// 	Serial.print("rsa message: "); Serial.println(buf);
	
// 	buf=RSA::crypt(buf,7,33);
// 	Serial.print("rsa rsaCrypt: "); Serial.println(buf);
// 	delay(300);
// 	buf=RSA::crypt(buf,3,33);
// 	Serial.print("rsa rsaDrypt: "); Serial.println(buf);
// 	Serial.println();
// 	i++;


  //Serial.print("rsa phi(n): "); Serial.println(rsa.phi);  
  //Serial.print("rsa phi(n): "); Serial.println(rsa.Phi());  

/*

  Serial.print("sizeof INT: "); Serial.print(sizeof(int)); Serial.println(" bytes"); 
  Serial.print("sizeof FLOAT: "); Serial.print(sizeof(float)); Serial.println(" bytes"); 
  Serial.print("sizeof DOUBLE: ");Serial.print(sizeof(uint32_t)); Serial.println(" bytes"); 
  Serial.print("sizeof uLOND: "); Serial.print(sizeof( long)); Serial.println(" bytes"); 
  Serial.print("sizeof LOND: "); Serial.print(sizeof(unsigned long)); Serial.println(" bytes"); 
  Serial.print("sizeof WORD: "); Serial.print(sizeof(word)); Serial.println(" bytes"); 
  Serial.print("sizeof long long "); Serial.print(sizeof(n)); Serial.println(" bytes");

*/
 
}

void loop() 
{
	
}

