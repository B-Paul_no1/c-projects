/*Scop:
	->De afisat card UID pe I2C_LCD
	c:\program files (x86)\arduino\hardware\tools\avr\avr\include\util\delay_basic.h
*/
#include <Wire.h>				//PT I2C
#include <LiquidCrystal_I2C.h>  //Pt LCD conectat prin port expander PCF 
#include <avr/wdt.h>			// PT WachDogTime
#include <SPI.h>				// includem protokolul de transmitere SPI
#include <MFRC522.h>			// pt RFID reader

#define SS_PIN 10
#define RST_PIN 9

/*optional*/
#define CH_per_LINE 16		//cite caractere pe linie
#define LINE_of_CHAR 2		// cite linii de caractere contine LCD-ul

MFRC522 mfrc522(SS_PIN, RST_PIN);  // Cream obiect obiect mfrc522 si transmitem constructorului param SS si RST pin

LiquidCrystal_I2C lcd(0x27,CH_per_LINE,LINE_of_CHAR);  // set the LCD address to 0x27(HHH)  adresa de I2C al I2C portExpander for a 16 chars and 2 line display

void (*resetFunc)(void)=0;  // soft reset chip
uint8_t k=0; 				// couter of reset

/* Start main */
// #############################################################################################
void setup(){

	SPI.begin();
	Serial.begin(9600);
	Serial.println(F("Card reader to serial "));

	lcd.init();                      // initialize the lcd 
	mfrc522.PCD_Init();				 // Initialize the MFRC522 

	// Print a message to the LCD
	lcd.backlight(); 
	lcd.print(F("  Start..."));
	// lcd.write(0x24); // char caracter $
    delay(1000);
	  
	  lcd.clear();
  	// wdt_enable(WDTO_4S);
}
// #############################################################################################
/* End main */



int cardDump(){
	lcd.clear();
	lcd.setCursor(0,0);
	lcd.print("Card UID:");
	lcd.setCursor(0,1);
	// lcd.noCursor();
	for (byte j=0; j<mfrc522.uid.size; j++){
		lcd.print(mfrc522.uid.uidByte[j],HEX);
		lcd.write(0x20);
		}
		return 255;
	}

/* Start loop */
// #############################################################################################
void loop(){ 
	byte r;
	wdt_reset(); // hardware reset chip
	byte uidCard[4]={0xD1, 0x49, 0x56, 0xC5};
	

	if ( !(! mfrc522.PICC_IsNewCardPresent() || ! mfrc522.PICC_ReadCardSerial()) ){
		cardDump();
		delay(1000);
		lcd.clear();
		lcd.setCursor(0,0);
		lcd.print(F("Function return:"));
		
		
		lcd.setCursor(0,1);
		lcd.print(r);
		delay(500);
		lcd.clear();
		lcd.setCursor(0,0);
		lcd.print(F("Serial print:"));
		lcd.setCursor(0,1);
		lcd.print(F("> Card dumping:"));
		wdt_reset();
		mfrc522.PICC_DumpToSerial(&(mfrc522.uid)); // PRint card dump to serial
		delay(500);
		lcd.setCursor(0,1);
		lcd.print(F("> END dumping:"));
		delay(1000);
		resetFunc();
		lcd.clear();
		return;
		}

//No card :	
	{
		lcd.setCursor(0,0);
		lcd.clear();
		lcd.print(F("Insert card:"));
		Serial.print(F("Baga cardul\n"));
		k++;

		if (k==4){
			k=0;
			Serial.println(F("Reset chip"));
			Serial.println(F("-----------------------------------------"));
			delay(500);
			resetFunc();
			}
		}


	delay(1000);
	lcd.clear();
	



}

/*
// Serial Dump Card Function:


void MFRC522::PICC_DumpToSerial(Uid *uid	///< Pointer to Uid struct returned from a successful PICC_Select().
								) {
	MIFARE_Key key;
	
	// UID
	Serial.print(F("Card UID:"));
	for (byte i = 0; i < uid->size; i++) {
		if(uid->uidByte[i] < 0x10)
			Serial.print(F(" 0"));
		else
			Serial.print(F(" "));
		Serial.print(uid->uidByte[i], HEX);
	} 
	Serial.println();
	
	// PICC type
	byte piccType = PICC_GetType(uid->sak);
	Serial.print(F("PICC futai: "));
	Serial.println(PICC_GetTypeName(piccType));
	
	// Dump contents
	switch (piccType) {
		case PICC_TYPE_MIFARE_MINI:
		case PICC_TYPE_MIFARE_1K:
		case PICC_TYPE_MIFARE_4K:
			// All keys are set to FFFFFFFFFFFFh at chip delivery from the factory.
			for (byte i = 0; i < 6; i++) {
				key.keyByte[i] = 0xFF;
			}
			PICC_DumpMifareClassicToSerial(uid, piccType, &key);
			break;
			
		case PICC_TYPE_MIFARE_UL:
			PICC_DumpMifareUltralightToSerial();
			break;
			
		case PICC_TYPE_ISO_14443_4:
		case PICC_TYPE_ISO_18092:
		case PICC_TYPE_MIFARE_PLUS:
		case PICC_TYPE_TNP3XXX:
			Serial.println(F("Dumping memory contents not implemented for that PICC type."));
			break;
			
		case PICC_TYPE_UNKNOWN:
		case PICC_TYPE_NOT_COMPLETE:
		default:
			break; // No memory dump here
	}
	
	Serial.println();
	PICC_HaltA(); // Already done if it was a MIFARE Classic PICC.
} // End PICC_DumpToSerial()


*/