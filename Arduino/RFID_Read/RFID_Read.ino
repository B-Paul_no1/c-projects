/*
 Citim data personala in memoria cardului
Pasi:
    1) Citirea unui masiv de date cu adresa respectiva 
    2) Citirea unui sector de date cu adresa respectiva 
  
  ###***************************************************###
  Huiova: Folosim biblioteca (okupa memorie)
  Bun: Ne invatam a scrie ce vrea muschiul nostru
  bukata de cod pizdita de la ReadAndWrite.ino 
  
  PICC type: MIFARE 1KB
  Sector Block 0  1  2  3   4  5  6  7   8  9 10 11  12 13 14 15  AccessBits

  15     63   00 00 00 00  00 00 FF 07  80 69 FF FF  FF FF FF FF  [ 0 0 1 ] 
         62   00 00 00 00  00 00 00 00  00 00 00 00  00 00 00 00  [ 0 0 0 ] 
         61   00 00 00 00  00 00 00 00  00 00 00 00  00 00 00 00  [ 0 0 0 ] 
         60   00 00 00 00  00 00 00 00  00 00 00 00  00 00 00 00  [ 0 0 0 ] 
                                    [...] 
   1      7   00 00 00 00  00 00 FF 07  80 69 FF FF  FF FF FF FF  [ 0 0 1 ] 
          6   00 00 00 00  00 00 00 00  00 00 00 00  00 00 00 00  [ 0 0 0 ] 
          5   20 20 20 20  20 20 20 20  20 20 20 20  20 20 65 01  [ 0 0 0 ] 
          4   50 61 73 20  20 20 20 20  20 20 20 20  20 20 20 20  [ 0 0 0 ]

   0      3   00 00 00 00  00 00 FF 07  80 69 FF FF  FF FF FF FF  [ 0 0 1 ] <-- trailerBlock
          2   20 20 20 20  20 20 20 20  20 20 20 20  20 20 65 01  [ 0 0 0 ] 
          1   50 61 63 68  61 20 20 20  20 20 20 20  20 20 20 20  [ 0 0 0 ] 
          0   59 E8 55 C5  21 88 04 00  85 00 B4 2E  F0 BB 6A A8  [ 0 0 0 ] <-- nu-l deranjam
             |    UID    |   


    MIFARE 1KB 16 sectors, 64 blocks             
 */

#include <SPI.h>
#include <MFRC522.h>

#define RST_PIN         9           // Configurable, see typical pin layout above
#define SS_PIN          10          // Configurable, see typical pin layout above


#define Auth_Key_A  MFRC522::PICC_CMD_MF_AUTH_KEY_A        
       

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

MFRC522::MIFARE_Key key;

byte count=0;
void setup() {
    
    Serial.begin(9600); // Initialize serial communications with the PC
    while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
    SPI.begin();        // Init SPI bus
    mfrc522.PCD_Init(); // Init MFRC522 card

    // in varianta codului chinez:
    //                  10   11   12  13   14    15
    // key.keyByte[6](0xFF,0xFF,0xFF,0xFF,0xFF,0xFF);
    
    // trbuie obligatoriiu
    for (byte i = 0; i < 6; i++) {  // trebuie daq nu PCD_Authenticate() failed
            key.keyByte[i] = 0xff;
                }


Serial.println(F("\t\t\t*** Program name: RFID_Write *** \n "));

Serial.println(F("Inceput"));
/* pina ce suspend 
    // Prepare the key (used both as key A and as key B)
    // Pregătiți cheia (folosit atât ca cheie A si ca cheie B)
    // using FFFFFFFFFFFFh which is the default at chip delivery from the factory
    

    Serial.println(F("Scan a MIFARE Classic PICC to demonstrate read and write."));
    Serial.print(F("Using key (for A and B):"));
    dump_byte_array(key.keyByte, MFRC522::MF_KEY_SIZE);
    Serial.println();
    
    Serial.println(F("BEWARE: Data will be written to the PICC, in sector #1"));
    */
}
/***************************** END setup() *****************************/        


void (*resetFunc)(void)=0;



/***************************** BEGIN Loop *****************************/        
void loop() {
    
    byte sector(0);
    byte blockAddr(0);    // adresa po defaultu=0
    
    byte trailerBlock(7); // unde se ascunde keya
    byte status;
    byte buffer[18];
    byte size(sizeof(buffer));
    byte piccType(mfrc522.PICC_GetType(mfrc522.uid.sak));
label_1:
// Autificare, optiune obligatorie
    mfrc522.PCD_Authenticate(Auth_Key_A, trailerBlock, &key,&(mfrc522.uid));


// Uita-te pentru noi carduri
    if ( (! mfrc522.PICC_IsNewCardPresent()) || (! mfrc522.PICC_ReadCardSerial()) )       return;

    
    Serial.print(F("Card UID:"));
    dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);  // print uid
    Serial.println();
      

    
/****************************** Read data from the sector **********************************************/
    

    // Ucoment pt a afisa pe terminal sectorul 
    // Serial.println(F("Current data in sector:"));
    // // functia de citire de reprentare a unui sector de date (4 blocuri de date)
    mfrc522.PICC_DumpMifareClassicSectorToSerial(&(mfrc522.uid), &key, sector);  
    Serial.println();




    
/********************************* Read block *******************************************/
    // Read data from the block (again, should now be what we have written)
  if(0)  


 {   blockAddr=1;
 
     mfrc522.MIFARE_Read(blockAddr, buffer, &size);
    
     Serial.print(F("Data in block ")); 
     Serial.print(blockAddr); 
 
     Serial.println(F(":"));
     dump_byte_array(buffer, 16); 
     Serial.println("\n \n ");
     
     // Serial.setTimeout(20000L) ; 
     // Serial.readBytesUntil('1', (char *) buffer, 18) ;
     Serial.println(F("'Pizdet  chip reset :)))\n"));
     delay(3000);
     // resetFunc();
   }


   if (count==1){
    resetFunc();

    // count=0;
    // goto label_1;


   }
   count++;
// label_1:

    // Halt PICC
    mfrc522.PICC_HaltA(); // regim de asteptare 
    // Stop encryption on PCD
    mfrc522.PCD_StopCrypto1();
// label1:

// {

//     asm("NOP");
//     delay(500);
//     Serial.println(label1);
// }

//    if (!mfrc522.uid.uidByte[0]==0x04){ 
//         goto label1;



//         }
}


/***************************** END Loop *****************************/        



/***************************** BEGIN dump_byte_array() *****************************/        
void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
        }
}
/***************************** END dump_byte_array() *****************************/        















/***************************** Original idea: \/ *****************************/        
// #include <SPI.h>
// #include <MFRC522.h>

// #define RST_PIN         9           // Configurable, see typical pin layout above
// #define SS_PIN          10          // Configurable, see typical pin layout above

// MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

// MFRC522::MIFARE_Key key;


// void setup() {
//     Serial.begin(9600); // Initialize serial communications with the PC
//     while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
//     SPI.begin();        // Init SPI bus
//     mfrc522.PCD_Init(); // Init MFRC522 card

//     // in varianta codului chinez:
//     //                  10   11   12  13   14    15
//     // key.keyByte[6](0xFF,0xFF,0xFF,0xFF,0xFF,0xFF);
    
//     // trbuie obligatoriiu
//     for (byte i = 0; i < 6; i++) {  // trebuie daq nu PCD_Authenticate() failed
//             key.keyByte[i] = 0xff;
//                 }

// /* pina ce suspend 
//     // Prepare the key (used both as key A and as key B)
//     // Pregătiți cheia (folosit atât ca cheie A si ca cheie B)
//     // using FFFFFFFFFFFFh which is the default at chip delivery from the factory
    

//     Serial.println(F("Scan a MIFARE Classic PICC to demonstrate read and write."));
//     Serial.print(F("Using key (for A and B):"));
//     dump_byte_array(key.keyByte, MFRC522::MF_KEY_SIZE);
//     Serial.println();
    
//     Serial.println(F("BEWARE: Data will be written to the PICC, in sector #1"));
//     */
// }
// /***************************** END setup() *****************************/        


// void (*resetFunc)(void)=0;



// /***************************** BEGIN Loop *****************************/        
// void loop() {

//     byte sector;
//     byte blockAddr; 
//     // byte dataBlock[16]  = {0xAA,0xAB,0xAC,0xAD,0xAE,0xAF};
    
//     byte trailerBlock   = 7; // unde se ascunde keya
//     byte status;
//     byte buffer[18];
//     byte size = sizeof(buffer);

    


//     // Look for new cards
//     if ( (! mfrc522.PICC_IsNewCardPresent()) || (! mfrc522.PICC_ReadCardSerial()) )       return;

//     // Select one of the cards
//     // if ( ! mfrc522.PICC_ReadCardSerial())
//     //     return;

//     // Show some details of the PICC (that is: the tag/card)
//     Serial.print(F("Card UID:"));
//     dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);  // print uid
//     Serial.println();
//     Serial.print(F("PICC type: "));
   

//     byte piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    
//     // Serial.println(mfrc522.PICC_GetTypeName(piccType));

//     // Check for compatibility
//     // if (    piccType != MFRC522::PICC_TYPE_MIFARE_MINI
//     //     &&  piccType != MFRC522::PICC_TYPE_MIFARE_1K
//     //     &&  piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
//     //     Serial.println(F("This sample only works with MIFARE Classic cards."));
//     //     return;
//     // }

//     // In this sample we use the second sector,
//     // that is: sector #1, covering block #4 up to and including block #7
   
    
// /****************************** Read data from the sector **********************************************/
//     // Authenticate using key A
//     // Serial.println(F("Authenticating using key A..."));
// status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 
//                                     trailerBlock,
//                                      &key, 
//                                     &(mfrc522.uid));
// if (status != MFRC522::STATUS_OK) {
//     Serial.print(F("PCD_Authenticate() failed: "));
//     Serial.println(mfrc522.GetStatusCodeName(status));
//     return;
//     }
//     Serial.println(F("Current data in sector:"));
//     // functia de citire de reprentare a unui sector de date (4 blocuri de date)
//     mfrc522.PICC_DumpMifareClassicSectorToSerial(&(mfrc522.uid), &key, sector);  
//     Serial.println();



// /****************************** Read data from the block **********************************************/
//     //
//     // Serial.print(F("Reading data from block ")); Serial.print(blockAddr);
//     // Serial.println(F(" ..."));
//     status = mfrc522.MIFARE_Read(blockAddr, buffer, &size);
//     if (status != MFRC522::STATUS_OK) {
//         Serial.print(F("MIFARE_Read() failed: "));
//         Serial.println(mfrc522.GetStatusCodeName(status));
//     }
//     // Serial.print(F("Data in block ")); 
//     // Serial.print(blockAddr); 
//     // Serial.println(F(":"));
//     // dump_byte_array(buffer, 16); Serial.println();
//     // Serial.println();
//     // delay(3000);

// /****************************** Authenticate using key B **************************/
// /*
//     Serial.println(F("Authenticating again using key B..."));
//     status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_B, trailerBlock, &key, &(mfrc522.uid));
//     if (status != MFRC522::STATUS_OK) {
//         Serial.print(F("PCD_Authenticate() failed: "));
//         Serial.println(mfrc522.GetStatusCodeName(status));
//         return;
//     }
// */


    
// /********************************* Read block *******************************************/
//     // Read data from the block (again, should now be what we have written)
//     blockAddr=0;
//     status = mfrc522.MIFARE_Read(blockAddr, buffer, &size);
//     // if (status != MFRC522::STATUS_OK) {
//     //     Serial.print(F("MIFARE_Read() failed: "));
//     //     Serial.println(mfrc522.GetStatusCodeName(status));
//     // }
//     Serial.print(F("Data in block ")); 
//     Serial.print(blockAddr); 

//     Serial.println(F(":"));
//     dump_byte_array(buffer, 16); 
//     Serial.println("\n \n ");
//     Serial.println("'Pizdet  chip reset :)))'");
//     delay(3000);
//     // resetFunc();

// /* // Check that data in block is what we have written
//     // by counting the number of bytes that are equal
//     Serial.println(F("Checking result..."));
//     byte count = 0;
//     for (byte i = 0; i < 16; i++) {
//         // Compare buffer (= what we've read) with dataBlock (= what we've written)
//         if (buffer[i] == dataBlock[i])
//             count++;
//     }
//     Serial.print(F("Number of bytes that match = "));
//     Serial.println(count);
// */
 
//     // Dump the sector data
//     /*
//     Serial.println(F("Current data in sector:"));
//     mfrc522.PICC_DumpMifareClassicSectorToSerial(&(mfrc522.uid), &key, sector);
//     Serial.println();
//     delay(2000);
//     */
//     // Halt PICC
//     mfrc522.PICC_HaltA(); // regim de asteptare 
//     // Stop encryption on PCD
//     mfrc522.PCD_StopCrypto1();
// }


// /***************************** END Loop *****************************/        



// /**
//  * Helper routine to dump a byte array as hex values to Serial.
//  */
// void dump_byte_array(byte *buffer, byte bufferSize) {
//     for (byte i = 0; i < bufferSize; i++) {
//         Serial.print(buffer[i] < 0x10 ? " 0" : " ");
//         Serial.print(buffer[i], HEX);
//         }
//     }
