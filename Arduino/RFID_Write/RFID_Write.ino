

#include <SPI.h>
#include <MFRC522.h>
#include <string.h>

#define RST_PIN         9           // Configurable, see typical pin layout above
#define SS_PIN          10 
#define LedOn digitalWrite(3, HIGH);         // Configurable, see typical pin layout above
#define LedOff digitalWrite(3, LOW);         // Configurable, see typical pin layout above

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

MFRC522::MIFARE_Key key;


byte dataBlock[16]    = {0xFF, byte('P')};
byte count=0; // program cycle



void setup() {
    Serial.begin(9600); // Initialize serial communications with the PC
    while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
    SPI.begin();        // Init SPI bus
    mfrc522.PCD_Init(); // Init MFRC522 card

// Подготовить ключ (ключ используется и как 'A' и B в качестве ключевых)
// using FFFFFFFFFFFFh which is the default at chip delivery from the factory
    for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
      }

  Serial.println(F("\t\t\t*** Program name: RFID_Write *** \n "));
    
  pinMode(7,INPUT);
  pinMode(3, OUTPUT);
  pinMode(2, OUTPUT);
  digitalWrite(2, LOW); //GND

   }

void (*reset)(void)=0;
   

//##################################################################
//########################### Start loop() #########################
//##################################################################
void loop() {
// Default String to write if not input to teminal
  char charArray[]={'P','a','c','h','a','$','$','\0','@'};  //0x03 - End of text
  /* Pacha in byte: 80  97  99 104  97   3    10  */ //0x0A - Line Feed
  /* Pacha in HEX:  50  61  63  68  61   3    A   */
  
  byte c=0x40;
  byte nullVector[16]={ 0x00,0x00,0x00,0x01,
                        0x00,0x00,0x00,0x02,
                        0x00,0x00,0x00,0x03,
                        0x00,0x00,0x00,0x04 };
  byte sector         = 1; 
  byte blockAddr      = 4;
  byte trailerBlock   = 7;
  byte status;
  byte buffer[18];
  byte size = sizeof(buffer);

 // digitalWrite(3, HIGH);
  LedOff;
// button state
  if(!digitalRead(7))
  {
    return;
  }
digitalWrite(3, HIGH);

    // Look for new cards
    if ( ! mfrc522.PICC_IsNewCardPresent())
        return;
      // Select one of the cards
    if ( ! mfrc522.PICC_ReadCardSerial())

        {
          return;
          count=0;
        }
    // Show some details of the PICC (that is: the tag/card)
    
    // Serial.println((char)c);
Serial.print(F(">>> Vector in HEX: "));
for (int i=0; i<sizeof(charArray); i++)
  {
    Serial.print(charArray[i],HEX); 
    Serial.print(" ");
    if (i==15) 
      Serial.println();

  }
Serial.println();
Serial.print(F("Cardu UID: "));
for (int i=0; i<sizeof(dataBlock); i++) {
    dataBlock[i]=0xFB;
    }


for (int i=0; i<sizeof(charArray); i++){

    dataBlock[i]=(byte)charArray[i];
   
    }


    dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
    Serial.println();
    Serial.print(F("PICC type: "));
    byte piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    Serial.println(mfrc522.PICC_GetTypeName(piccType));

    // Authenticate using key A
status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 
                                    trailerBlock, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
      Serial.print(F("PCD_Authenticate() failed: "));
      Serial.println(mfrc522.GetStatusCodeName(status));
      return;
  }
  Serial.println();

// inscriem vector null
 mfrc522.MIFARE_Write(blockAddr, nullVector, 16);
  delay(100);
Serial.println(">>> Write nullVector: ");  
MIFARE_readBlock(blockAddr);
// SeriaDumpBlock(blockAddr,1);

Serial.println();
Serial.println("Write personal data: "); 

// Write data to the block
if (count==0)
{
  Serial.println(F("############ Writing block ############ "));
  Serial.print(F("Writing data into block ")); 
  Serial.println(blockAddr);
 
  // dump_byte_array(dataBlock, 16); Serial.println();
  status = mfrc522.MIFARE_Write(blockAddr, dataBlock, 16);
    if (status != MFRC522::STATUS_OK) {
      Serial.print(F("MIFARE_Write() failed: "));
      Serial.println(mfrc522.GetStatusCodeName(status));
    }
  Serial.println();
}
count++;


Serial.print(F("Current Data:"));
MIFARE_readBlock(blockAddr);



     
/************************ Serial.write(buffer[1])******************************/

SeriaDumpBlock(blockAddr,1);        //=>0x40
    // Serial.println();

Serial.print("\nAfter write:");
MIFARE_readBlock(blockAddr);
// if ( ! mfrc522.PICC_IsNewCardPresent())        return;

    // Dump the sector data
    // Serial.println(F("Current data in sector:"));
    // mfrc522.PICC_DumpMifareClassicSectorToSerial(&(mfrc522.uid), &key, sector);
    // Serial.println();
    // delay(2000);
  
    
    // Halt PICC          // Stop encryption on PCD
    mfrc522.PICC_HaltA(); mfrc522.PCD_StopCrypto1();
   LedOn;
    delay(500); 
    reset();
    
}
//##################################################################
//########################### END loop() ###########################
//##################################################################




void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? "" : " ");
        Serial.print("0x"); Serial.print(buffer[i], HEX);
    }
}



/* Function Name: MIFARE_readBlock()
 * Function: 
 *      Parametrii de intrare:
 *          [block] - nr vectorului (masivului)
 *          [element] - pozitia elementului in masiv
 */
void MIFARE_readBlock(byte blockAdr)
{
  byte buffer[18];
  byte size=sizeof(buffer);

  mfrc522.MIFARE_Read(blockAdr, buffer, &size);
  Serial.print(F("\nData's in block ")); Serial.print(blockAdr); 
  Serial.println(F(":"));

  for (byte i = 0; i < 16; i++) {
          // Serial.print(' ');
          Serial.print(buffer[i] < 0x10 ? " 0x0" : " 0x");
          Serial.print(buffer[i], HEX);
        }
Serial.println();
delay(1000);

}


/* Function Name: SeriaDumpBlock()
 * Function: 
 *      Parametrii de intrare:
 *          [block] - nr vectorului (masivului)
 *          [element] - pozitia elementului in masiv
 */
inline void SeriaDumpBlock(byte block, byte element) {
  
  byte status;
  byte buff[18];
  byte size = sizeof(buff);

  /********************************************************/
  delay(100);

  status = mfrc522.MIFARE_Read(block, buff, &size);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("MIFARE_Read() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
         }

  Serial.print(F("\n>>> Vector read:  ")); 
  byte j=0;
  while(!(buff[j]=='\0')){

        Serial.print(char(buff[j]));
        j++;
      }

  Serial.print(F(" <<< End of text >>>\n"));
  delay(50);

}
;