

#include <SPI.h>
#include <MFRC522.h>
#include <string.h>

#define RST_PIN         9           // Configurable, see typical pin layout above
#define SS_PIN          10          // Configurable, see typical pin layout above

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

MFRC522::MIFARE_Key key;


byte dataBlock[16]    = {0xFF, byte('P')};

/*{
        0xbb, 0x40, 0x03, 0x04, //  1,  2,   3,  4,  0x03- end of text
        0x05, 0x06, 0x07, 0x08, //  5,  6,   7,  8,  0x02- start of text
        //0x08, 0x09, 0xff, 0x0b, //  9, 10, 255, 12,
        0x0c, 0x0d, 0x0e, 0x2f  // 13, 14,  15, 16
        };*/
byte count=0; // program cycle



void setup() {
    Serial.begin(9600); // Initialize serial communications with the PC
    while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
    SPI.begin();        // Init SPI bus
    mfrc522.PCD_Init(); // Init MFRC522 card

// Подготовить ключ (ключ используется и как 'A' и B в качестве ключевых)
// using FFFFFFFFFFFFh which is the default at chip delivery from the factory
    for (byte i = 0; i < 6; i++) {
        key.keyByte[i] = 0xFF;
      }

    Serial.println(F("\t\t\t*** Program name: RFID_Write *** \n "));


   }


void loop() {
// Default String to write if not input to teminal
  char charArray[]={'P','a','c','h','a',0x03,'\0'};  //0x03 - End of text
  /* Pacha in byte: 80 97 99 104 97 0 0 0 0 0 0 0 0 0 0 0 */
  
  byte c=0x40;
  byte biff[16];
  byte sector         = 1;
  byte blockAddr      = 4;
  
  byte trailerBlock   = 7;
  byte status;
  byte buffer[18];
  byte size = sizeof(buffer);


    // Look for new cards
    if ( ! mfrc522.PICC_IsNewCardPresent())
        return;
  Serial.print(F(">>> Vector for write:  "));
  Serial.println(charArray);
  Serial.print(F(">>> Vector size:  "));
  Serial.println(sizeof(charArray));
    // Select one of the cards
    if ( ! mfrc522.PICC_ReadCardSerial())

        {
          return;
          count=0;
        }
    // Show some details of the PICC (that is: the tag/card)
    
    // Serial.println((char)c);
    Serial.print(F(">>> Vector in HEX: "));



for (int i=0; i<sizeof(biff); i++)
  {
    Serial.print(biff[i]);Serial.print(" ");
    if (i==15) 
      Serial.println();

  }
Serial.println();
Serial.print(F("Card UID: "));

    // Serial.print(biff[0]);Serial.print(" ");
    // Serial.print(biff[1]);Serial.print(" ");
    // Serial.print(biff[2]);Serial.print(" ");
    // Serial.print(biff[3]);Serial.print(" ");
    // Serial.print(biff[4]);Serial.println(" ");

for (int i=0; i<sizeof(dataBlock); i++)
  {
    dataBlock[i]=32;
    }

for (int i=0; i<sizeof(charArray); i++)
  {
    dataBlock[i]=(byte)charArray[i];
    }

    dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);
    Serial.println();
    Serial.print(F("PICC type: "));
    byte piccType = mfrc522.PICC_GetType(mfrc522.uid.sak);
    Serial.println(mfrc522.PICC_GetTypeName(piccType));

    
    




    // Authenticate using key A
status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 
                                    trailerBlock, &key, &(mfrc522.uid));
  if (status != MFRC522::STATUS_OK) {
      Serial.print(F("PCD_Authenticate() failed: "));
      Serial.println(mfrc522.GetStatusCodeName(status));
      return;
  }
Serial.println();

/*
// afisam in terminal adresa informatiei:
Serial.println(F("Selected:"));
Serial.print(F(">>> Sector: ")); Serial.println(sector);
Serial.print(F(">>> Block:  ")); Serial.println(blockAddr);
Serial.println();
*/


  // Show the whole sector as it currently is
  Serial.println(F("Current data in sector:"));
  mfrc522.PICC_DumpMifareClassicSectorToSerial(&(mfrc522.uid), &key, sector);
  Serial.println();
    

Serial.println("Write personal data: ");

Serial.setTimeout(20000L) ;      // rabdam 20 sec
Serial.readBytesUntil('1', (char *) dataBlock, 16);
    
// Write data to the block
if (count==0){
  Serial.println(F("############ Writing block ############ "));
  Serial.print(F("Writing data into block ")); Serial.println(blockAddr);
  // dump_byte_array(dataBlock, 16); Serial.println();
  status = mfrc522.MIFARE_Write(blockAddr, dataBlock, 16);
    if (status != MFRC522::STATUS_OK) {
      Serial.print(F("MIFARE_Write() failed: "));
      Serial.println(mfrc522.GetStatusCodeName(status));
    }
  Serial.println();
    }
count++;

/*
    // Read data from the block (again, should now be what we have written)
    Serial.println(F("############ Reading block ############ "));
    Serial.print(F("Reading data from block ")); Serial.print(blockAddr);
    Serial.println(F(" ..."));
    status = mfrc522.MIFARE_Read(blockAddr, buffer, &size);
    if (status != MFRC522::STATUS_OK) {
        Serial.print(F("MIFARE_Read() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
    }
   */

mfrc522.MIFARE_Read(blockAddr, buffer, &size);
Serial.print(F("Data in block ")); 
Serial.print(blockAddr); 
Serial.println(F(":"));
dump_byte_array(buffer, 16); 
Serial.println();
delay(1000);
     
/************************ Serial.write(buffer[1])******************************/

                   // ch     block       temp bufer 
    // printDumpArray_1( 15,  blockAddr,   buffer); // print "/"
   


    // Serial.println(F("   >>>SeriaDumpBlock(byte block, byte element):"));
    SeriaDumpBlock(blockAddr,1);        //=>0x40
    // Serial.println();

    
// if ( ! mfrc522.PICC_IsNewCardPresent())        return;

    // Dump the sector data
    // Serial.println(F("Current data in sector:"));
    // mfrc522.PICC_DumpMifareClassicSectorToSerial(&(mfrc522.uid), &key, sector);
    Serial.println();
    delay(2000);
    // return;
    // Halt PICC
    mfrc522.PICC_HaltA();
    // Stop encryption on PCD
    mfrc522.PCD_StopCrypto1();
}
//##################################################################
//########################### END loop() ###########################
//##################################################################

/*Cod chinez:
    // Serial.print(biff[0]);Serial.print(" ");
    // Serial.print(biff[1]);Serial.print(" ");
    // Serial.print(biff[2]);Serial.print(" ");
    // Serial.print(biff[3]);Serial.print(" ");
    // Serial.print(biff[4]);Serial.println(" ");

*/




void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
    }
}


/* Function Name: SeriaDumpBlock()
 * Function: 
 *      Parametrii de intrare:
 *          [block] - nr vectorului (masivului)
 *          [element] - pozitia elementului in masiv
 */
void SeriaDumpBlock(byte block, byte element) {
  
  byte status;
  byte buff[18];
  byte size = sizeof(buff);

  /********************************************************/
  delay(100);

  status = mfrc522.MIFARE_Read(block, buff, &size);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("MIFARE_Read() failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
         }

  // if ( element <=size )
  Serial.print(F("\nChar: "));
  Serial.write(buff[element]); 

  Serial.println();
 
  Serial.print(F("Hex: "));
  Serial.println(buff[element],HEX); 

  Serial.println(F("\n>>> Vector read: \n")); 
  byte j=0;
  while(!(buff[j]==0x03)){

        Serial.print(char(buff[j]));
        j++;
      }

  Serial.print(F("\n>>> End of text"));
  delay(50);

}
;