
// розеточный монстр

#include <SPI.h>
#include <MFRC522.h>

#define SS_PIN 53
#define RST_PIN 9
MFRC522 mfrc522(SS_PIN, RST_PIN);       // Create MFRC522 instance.
unsigned long uidDec, uidDecTemp;


#include <RCSwitch.h>

RCSwitch mySwitch = RCSwitch();

boolean switchOn = false;
boolean cardRead = false;

void setup() {
        Serial.begin(9600);     // Initialize serial communications with the PC
        SPI.begin();                    // Init SPI bus
        mfrc522.PCD_Init();     // Init MFRC522 card
        mySwitch.enableTransmit(8);
        Serial.println("Waiting for card...");
}

void loop() {
        byte status;
        byte byteCount;
        byte buffer[2]; // длина массива (16 байт + 2 байта контрольная сумма) 
        
        byteCount = sizeof(buffer);
        uidDec = 0;
        status = mfrc522.PICC_RequestA(buffer, &byteCount); 
          if (mfrc522.PICC_ReadCardSerial()) {
                  for (byte i = 0; i < mfrc522.uid.size; i++) {  
                     uidDecTemp=mfrc522.uid.uidByte[i];
                     uidDec=uidDec*256+uidDecTemp;
                  }
                  if ((uidDec==2218415941) && (switchOn == false)) {
                    mySwitch.sendTriState("00110000F000");
                    switchOn = true;
                    // Serial.println("Switched On");
                  }                          
                  mfrc522.PICC_ReadCardSerial();
                  
        } else {
          if (switchOn == true) {
            mySwitch.sendTriState("001100000000");
            // Serial.println("Switched Off");
            switchOn = false;
          }
        }
 }
