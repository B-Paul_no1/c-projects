

#define buffMaxSize 80  //nr maxim de caractere pt mem tampon
#define SOH 0x01        //Start Of Heading
#define SOT 0x02        //Start Of Text
#define ETX 0x03        //End Of Text
#define EOT 0x04        //End Of Transmission
#define CR  0x0D        //Carriage Return '\n'
#define LF  0x0A        //Line Feed
typedef uint8_t byte;


uint8_t dataTransive(uint8_t bd[], 
                     uint8_t arrLen=80)     //aratam lungimea masivului
{
  for (int i=0; i<arrLen; i++)
  {
    Serial.write(bd[i]);
    if (bd[i]==EOT) break;
    
  }
  return 0;
}




byte (*ptrF)(byte[],byte)=&dataTransive;

enum  atCommand {AT=   0x80,  //128,     //comanda rpincipala
                 atPID=0x81,  //129
                 atVID=0x82,  //130
                 atUID=0x83,  //131
                 atSOH=0x01   //132
                //atUID=0x82,
                };


/*  Organizarea containerului de date bData
                  0    1-76  77  78  79
        bData[]={SOH, [...], CR, LF, EOT}
*/
byte bData[buffMaxSize]={0};

// int a=0;

// byte *Contact=new byte[1024];



void setup() 
{
  bData[0]=SOH;
  bData[79]=EOT;    //sfirsit obligatoriu

  //bData[1]=56;   //0x83  
  bData[1]=atVID;  //0x83  
  bData[2]='P';    //0x50
  bData[3]='A';    //0x41
  bData[4]='V';    //0x56
  bData[5]='E';    //0x45
  bData[6]='L';    //0x4C
  bData[7]=LF;     //0x0A
  bData[8]='K';    //0x4B 75
  bData[9]=EOT;    //0x04
  bData[10]='M';   //0x4D 77



  //miniSimulation
  //            193 210   243 
  byte VID[3]={0xC1,0xD2,0xF3}; // Vendor  ID (sim)
  byte PID[3]={0x03,0x03,0x03}; // Product ID (sim)


  bData[2]=VID[0];
  bData[3]=VID[1];
  bData[4]=VID[2];

  //Initialize serial and wait for port to open:
  Serial.begin(9600);

  //Serial.println("ASCII Table ~ Character Map");

  dataTransive(bData);

  delay(2000);


  Serial.write(EOT);
  Serial.end();

  // delete[] Contact;

}


void loop() 
{
  delay(100);
}