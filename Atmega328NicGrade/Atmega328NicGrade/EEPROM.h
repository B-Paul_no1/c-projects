#ifndef EEPROM_H_
#define EEPROM_H_

#include <avr/io.h>

typedef unsigned char byte;

#define eepromStartAddr		0x000
#define eepromEndAddr		0x3FF // 1Kb [1023]

byte eepromRead(unsigned int address)	
{
	while (EECR & (1<<EEPE)) {};	//wait  for completion of previous write 
	EEAR=address;					//set address  register 
	EECR|=(1<<EERE);				// set enable read 
	return EEDR;					// return read value
		
}

void eepromWrite(	unsigned int address,
					byte data)
{
	while (EECR & (1<<EEPE));	// wait previous operation
	EEAR=address;				// set address register
	EEDR=data;					// set data register
	
	EECR|=(1<<EEMPE);			// EEPROM Mater enable
	EECR|=(1<<EEPE);			// EEPROM write enable 
}

void eepromWriteBlock(unsigned int address,
					  byte *block,
					  unsigned int blockSize=1)
{
	for(int i=0; i<blockSize; i++)
	{
		
		eepromWrite(address,*block);
		address++;
		block++;
	}
	
}

bool eepromErase()
{
	for(unsigned int i=0; i<=eepromEndAddr; i++)
	{
		eepromWrite(i,0xFF);
	}
	
}






#endif