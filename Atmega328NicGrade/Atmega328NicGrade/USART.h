/*
 * USART.h
 * 
 * Created: 21.07.2016 16:30:24
 *  Author: Pavel
 * Serial Communication 
 */ 


#ifndef USART_H_
#define USART_H_


class USART
{
private:
	unsigned int ubrr;
	unsigned long Fcpu;
public:
	//constructor
	USART()
	{
		#ifndef F_CPU
			#define F_CPU 16000000ul
		#endif
		Fcpu=F_CPU;
	}
	~USART()
	{
		while(0);
		
		
	}
	
	void begin(unsigned int baudRate)
	{
		this->ubrr=Fcpu/16/baudRate-1;
		//set baud Rate
		UBRR0H=(unsigned char)(this->ubrr>>8);
		UBRR0L=(unsigned char)(this->ubrr);		
		
		// Enable receiver and transmitter 
		UCSR0B=(1<<RXEN0)|(1<<TXEN0);
		
		// Set frame: 8data, 2stop bit
		UCSR0C=(1<<USBS0)|(3<<UCSZ00);
	}
	void write(unsigned char data) //sending frames with 5 to 8 Data Bit 
	{
		//wait for empty transmit buffer
		while (!(UCSR0A & (1<<UDRE0)));
		
		//Put data into buffer, sends the data
		UDR0=data;
	}
	//send string 
	void print(char *str)
	{
		while(*str !='\0')
		{
			this->write(*str);
			str++;
		}
	}
	//send string with CR(carriage retur)
	void println(char *str)
	{
		
		this->print(str);
		this->write(0x0D); // new line 
	}
	//overload println()
	void println(void)
	{
		this->write(0x0D);
	}
	
	//receive one byte of data
	unsigned char read(void)
	{
		while(!(UCSR0A & (1<<RXC0)));
		return UDR0;
	}
	// Flushing the Receive Buffer
	void flush(void)
	{
		unsigned char dummy;
		while(UCSR0A & (1<<RXC0)) 
			dummy=UDR0;
	}
	
	
}Serial;



#endif /* USART_H_ */