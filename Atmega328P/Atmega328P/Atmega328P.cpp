/*
 * Atmega328P.cpp
 *
 * Created: 13.07.2016 13:37:54
 *  Author: Pavel
 *  include Proteus simulation projects
 *
 */ 

#define F_CPU 8000000ul	//8Mhz internal osc

#include <avr/io.h>]
#include <avr/interrupt.h>
#define LedOk	PD3
#define BtPin	PC1





int main(void)
{	
	cli();
	DDRC&=~(1<<BtPin);
	DDRD&=~(1<<PD2);				//PD2 input
	DDRD|=(1<<PD3)|(1<<PD7);		//pin 1 output
	
	PORTD&=~(1<<PD3);				// enable Pull-Up res
	
	EICRA|=(1<<ISC01)|(1<<ISC00);	//rising edge 
	EIMSK|=(1<<INT0);				//enable extIterr INT0
	
	sei();			//enable interrupts
	
	while (1)
	{ 
		if(PINC&(1<<BtPin))
		{
			PORTD^=(1<<PD3);
			
		}
	}
	
	
    
}

ISR(INT0_vect)
{
	//asm("cli");
	
	PORTD^=(1<<PD3);
	PORTD&=~(1<<PD7);
	
	
}