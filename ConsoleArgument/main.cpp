#include <iostream>
#include <string.h>
//#include <ctype.h>

using namespace std;


int main(int argc,char* argv[])
/*
    argc   - nr de parametrii tranmise functiei main
    argv[] - masiv de pointeri, se transmite
    argv[0] - masiv de pointeri, se transmite argumente
    Sintaxa:
    C:\>[nume_programa].[extensie] argument_1 ... argument_n
*/

{

    if (argc>1)
    {
        if (0==strcmp(argv[1],"AT")) cout<<"\nAT OK"<<endl;


        cout<<"argc= "<<argc<<endl;
        //cout<<"strcmp= "<<strcmp(argv[1],"AT")<<endl;
        cout<<"\tProgramm name: "<<argv[0]<<endl;
        //cout<<argv[1]<<endl;

        for (int i=1;i<argc;i++)
            cout<<">> "<<argv[i]<<endl;
    }
    else
        cout<<"Lipsa argument"<<endl;
    //system("pause");
    return 0;
}
