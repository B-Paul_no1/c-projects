#ifndef ALLFUNCTION_H_INCLUDED
#define ALLFUNCTION_H_INCLUDED

#include <iostream>
#include <stdlib.h>
#include "totFigneauaAcolo.h"

typedef uint8_t byte;

using namespace std;




byte hexToDec(char in)
{
    // for 0..9
    if (in>47 && in<58) return in-'0';
    //for A..F
    if (in>64 && in<71) return in-55;
}

void decodeLine(char* inBuff)
{

   int dLen;  //  byte of Data
   int crc;    // Checksum

   byte a;


    if (inBuff[0] != ':')
    {
        cout<<"nui nica"<<endl;
        return;
    }


    dLen=hexToDec(inBuff[1])*16;
    dLen+=hexToDec(inBuff[2]);



    crc=hexToDec(inBuff[9+dLen*2])*16;
    crc+=hexToDec(inBuff[10+dLen*2]);




    cout<<"\ndata len "<<dLen<<endl;

//    cout<<inBuff[9]<<endl;

    int *data=new int[dLen];

    for (int i=0; i<dLen*2; i++)
//    for (int i=0; i<4; i++)
    {
        static byte idx=0;

      cout <<inBuff[i+9];
//      cout <<inBuff[i+9]+0<<" ";
      if (i%2)
      {

        data[idx]=hexToDec(inBuff[i+8])*16;

        data[idx]+=hexToDec(inBuff[i+9]);

        idx++;

        cout<<endl;
      }

    }

    cout<<"dataRecord:"<<endl;
    for (int i=0; i<dLen; i++)
    {

        cout<<data[i]<<" ";
    }
    cout <<"\nCRC= "<<hex<<uppercase<<"0x"<<crc<<dec<<endl;

//    cout<<"Calculed CRC="<<calcCRC(data, dLen)+0<<endl;

    delete data;

}

#endif // ALLFUNCTION_H_INCLUDED
