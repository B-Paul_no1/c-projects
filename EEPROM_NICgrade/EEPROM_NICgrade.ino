
#define eepromStartAddr 	0x000
#define eepromEndAddr 		0x3FF	/// 1023

#define startContactAdrr 	0xE0 	
#define myUIDadtddr 		0xDC 	// 	220	
#define myUidFlagAddr		0xDB
#define cPTR				0x00
#define savePTR(x) {eepromWrite(0x20,x>>8); eepromWrite(0x21,x);}

#define BtPin 		PC1 //	A1
#define LedOk 		PD3	//	3
#define CoilOff 	PD4	// 	4



unsigned char eepromRead(unsigned int address)
{
	while(EECR&(1<<EEPE)){}	//wait for completion of previous write
	EEAR=address;			//set address  register
	EECR|=(1<<EERE);
	return EEDR;
}

void eepromWrite(	unsigned int address, 
					unsigned char data)
{
	/* Wait for completion of previous write */
	while(EECR & (1<<EEPE));
	EEAR = address;		//set address  register 2bytes
	EEDR = data;		//set data register
	/* Write logical one to EEMPE */
	EECR |= (1<<EEMPE);
	/* Start eeprom write by setting EEPE */
	EECR |= (1<<EEPE);

}

void eepromWriteBlock(	unsigned int address,
						char *block,
						unsigned int blockSize=1)
{
	for (int i=0; i<blockSize; i++)
	{
		eepromWrite(address,*block);
		address++;
		block++;
	}

}

void eepromErase()
{
	for (unsigned int i=0; i<=eepromEndAddr; i++)
	{
		eepromWrite(i,0xFF);
	}
}



char UID[4]={0xCE, 0x1B,0xF3, 0x69};



unsigned int ptr=0x0E0; //
byte n;

int val = 0;
uint8_t cCT;	//contact Counter




void setup()
{
	// DDRD|=0;
	// cli();			//// disable global interrupts

	// //External Interrupt Control Register
	// EICRA|=(1<<ISC01)|(0<<ISC00);	// INT0 falling ,frontul de coborire]
	// // External Interrupt Mask Register
	// EIMSK|=(1<<INT0); //Enabel INT0
	cCT=eepromRead(countContactAdrr);
	cCT++;
	eepromWrite(countContactAdrr,cCT);


	pinMode(PD3, OUTPUT);
	pinMode(BtPin, INPUT);
	// digitalWrite(PD3, HIGH);
	delay(200);

	char fl=eepromRead(myUidFlagAddr);
	digitalWrite(LedOk, HIGH);   // turn the LED on (HIGH is the voltage level)
	delay(1000);              // wait for a second
	digitalWrite(LedOk, LOW);    // turn the LED off by making the voltage LOW
	delay(1000);
	 
	if(!fl)
	{	
		eepromWriteBlock(0xDC,UID,4);
		eepromWrite(myUidFlagAddr, 0xFF);
		
		eepromWriteBlock(0x40,"Manoli Pavel", 12);
		eepromWriteBlock(0x50,"NicGrade SRL", 12);

		digitalWrite(LedOk, HIGH);   // turn the LED on (HIGH is the voltage level)
		delay(1000);              // wait for a second
		digitalWrite(LedOk, LOW);    // turn the LED off by making the voltage LOW
		delay(1000); 
	}

	// sei(); //global interrupt enable

	
	// eepromWrite(startContactAdrr, '#');
	
	// for (int i=0; i<32; i++)	
	// {
	// 	eepromWriteBlock(ptr, UID, 4);
	// 	ptr+=4;

	// }
	// eepromWrite(0x31,ptr);
	// eepromWrite(0x30,ptr>>8);
	
	// eepromWrite(0x40,ptr>>8); 
	// eepromWrite(0x41,ptr);
	
		
	// savePTR(0x160);
	// 	//
	// // n=eepromRead(0xe0);
	// // eepromWrite(0x31,n);

	// // eepromWriteBlock(0x08, "A", 1);
	// // eepromWriteBlock(0x09, &f);
	// // eepromErase();		
 //  	digitalWrite(PD3, LOW);
	// int pinR;
 //  	digitalWrite(LedOk, HIGH);
 //  	while(1)

 //  	{
 //  	    // pinR=digitalRead(BtPin);
	// digitalWrite(LedOk,LOW);
	// // if (pinR)
	// // {
	// // 	addContact();
	// // 	delay(10);
	// // }
	// delay(500);
	
 //  	}
}


void ledBlink()
{
	while (true)	{
	digitalWrite(LedOk, HIGH);   // turn the LED on (HIGH is the voltage level)
		delay(1000);              // wait for a second
		digitalWrite(LedOk, LOW);    // turn the LED off by making the voltage LOW
		delay(1000);  }
}

void loop() 
{
	// pinR=digitalRead(BtPin);
	// 
	// // if (pinR)
	// // {
	// // 	addContact();
	// // 	delay(10);
	// // }
	// val = digitalRead(BtPin);   // read the input pin
	// digitalWrite(LedOk, !val);    // sets the LED to the button's value
	

	// digitalWrite(LedOk, HIGH);   // turn the LED on (HIGH is the voltage level)
	// delay(1000);              // wait for a second
	// digitalWrite(LedOk, LOW);    // turn the LED off by making the voltage LOW
	// delay(1000);  
}
// void addContact()
// {
// 	digitalWrite(PD3, HIGH);
// 	int ct=eepromRead(0x00);
// 	ct++;
// 	eepromWrite(0x00,ct);
// 	delay(500);
// 	digitalWrite(PD3, LOW);
// }
ISR(INT0_vect)
{
	ledBlink();
}
