#ifndef RSA_H
#define RSA_H

//RSA encrypt / decrypt
class RSA
{
    public:
        //constructor:
        RSA();


        //daq nr este prim?:
        static bool IsPrime(int num);

        // (b*p)%m
        static long BigMod( int b, int p, int m);

        //encrypt function:
       // string encrypt(string text);

        long RSA::BigMod(int b, int p, int m);

        int RSA::n_value(int prime1, int prime2);

        static int RSA::phi();

        static int32_t RSA::privateKey(int phi, int e, int n);


        //destructor
        ~RSA();
    protected:
    private:
};

#endif // RSA_H
