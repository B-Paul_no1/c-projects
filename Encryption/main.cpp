#include <iostream>
#include <stdio.h>
#include <math.h>
#include <string>
#include <conio.h>
#include <stdlib.h>



using namespace std;

class Human
{
    //constructor
public: string Name;
        int Age;

        Human(){}


public: Human(string Name, int Age)
    {
        this->Name=Name;    // this->Name apel la var in afara metodei
        this->Age=Age;

      //  cout<<"Age ="<<Age<<endl;
        cout<<"apel constructor Human\n";
    }

    public: void  Print()
    {
        cout<<endl;
        cout<<"Name: "<<Name<<endl;
        cout<<"Age: "<<Age<<endl;
    }




    //destructor
  //public:
    ~Human()
    {
        //delete Age;
    }
};

namespace L3
{
    class Man: public Human
    {
        public: int Size;


        //constructor:
        public:  Man(string Name, int Age, int Size):Human::Human(Name,Age)
            {
                this->Name=Name;
                this->Age=Age;
                this->Size=Size;
            }

        public: void virtual Print()
        {
            cout<<endl;
            cout<<"Name: "<<Name<<endl;
            cout<<"Age:  "<<Age<<endl;
            cout<<"Size: "<<Size<< endl;
        }


        //destructor
        ~Man(){}
    };
}

using namespace L3; //pt clasa Man

void printHuman(Human* hum)
{

   cout<<"\nName: "<<hum->Name<<endl;
   cout<<"Age: "<<hum->Age<<endl;

    delete hum;
}


namespace rsa
{
    class RSA
    {

    public:

        int prim1,prim2=0;
        int e=0;



        //Constructor fara parametrii
        RSA()
        {

        }
    public: RSA(int prim1, int prim2)
        {
            this->prim1=prim1;
            this->prim2=prim2;
            cout<<"\nn value= "<<nValue(prim1,prim2)<<endl<<endl;

        }
        //calc n
       static int nValue(int p1,int p2)
        {
            return ( p1*p2);
        }

       static  int Phi(int p1, int p2)
        {
            return ((p1-1)*(p2-1));
        }

        //daca e nr prim
       static bool IsPrime(int num)
        {
            int  radacina=(int)sqrt((double)num);
            if (num<2) return false;
            if (num%2==0) return (num==2);

            for (int i=3; i<=radacina; i+=2)
            {
                if(num%i==0) return false;
            }
            return true;

        }

        //Alg Delfie_Helman (b^p)mod(m)
        static unsigned int crypt(int mesaj, int putere, int modul)
        {
            if (putere == 0) return 1;
            else if(putere%2 ==0) return (long)(pow(crypt(mesaj, putere/2,modul),2))%modul;
                 else return((mesaj%modul)*crypt(mesaj,putere-1, modul))%modul;
        }


        static long crypt_a(int mesaj, int putere, int modul)
        {
            long buff=0;
            buff=(long)pow(mesaj,putere)%modul;
            return buff;
        }
    static int privateKey(int phi, int e, int n)
    {
        int D, res = 0;
				for (D = 1;; D++)
				{
					res = (D*e) % phi;
					if (res == 1) break;
				}
				return D;
    }

        // phi()
        int phi()
    {
        return (this->prim1-1)*(this->prim2-1);
    }






        //Destructor
        ~RSA()
        {

        }

    };
}

//Exempl de structura:
struct myStruct //
    {
        /* Pasi:
            1 declaram structura in program : myStruct struct
            2. Accesare : struct.name =9
        */

        int name=9;
        int nams=9;
        char chr;
    };

volatile int  algDelfieHellman(int mes, int exp, int modul)
    {
        long buff=0;
        buff=(int)(pow(mes,exp));// % modul;
       // buff=buff % modul;
        return buff;

    }
using namespace rsa;

int main()
    {

        int *p;
        char* text;
        text="aaa";

        cout<<"\nChar* "<<text<<endl;


        p=(int*)malloc(4);
        //char text[3]={'i',''b','\0'};
        int x;

        myStruct struc;

        cout<<"\n x= "<<x<<endl;
        cout<<"\n x<<3: "<<(1<<3)<<endl; //1<<3=8
        cout<<" x>>3: "<<(96>>3)<<endl; //69>>3=12

        cout<<"\nMy struct: "<<struc.name<<endl;
        cout<<"\nMy struct: "<<x<<endl;



        cout<<"\nsize int: "<<sizeof(int)<<"=> "<<pow(2,sizeof(int)*8)<<endl;
        cout<<"size Long: "<<sizeof(long)<<"=> "<<pow(2,sizeof(long)*8)<<endl;
        cout<<"size Double: "<<sizeof(long double)<<"=> "<<pow(2,sizeof(double)*8)<<endl;
        cout<<"size struc: "<<sizeof(struc)<<"=> "<<pow(2,sizeof(struc)*8)<<endl;
        cout<<"size Rsa class: "<<sizeof(RSA)<<"=> "<<pow(2,sizeof(RSA)*8)<<endl;






    /*


     x=algDelfieHellman(x,7,33)

        int key,n,e,fi=0;
        int p1,p2=0;
        int x=5;
        Human h("vasea",20);




        p1=3; p2=11;
        e=7;

        //{7,33} public key
        //{3,33} private key

        n=RSA::nValue(p1,p2);
        fi=RSA::Phi(p1,p2);
        key=RSA::privateKey(fi,e,n);
        RSA mesaj(p1,p2);

        cout<<"\nphi("<<p1<<", "<<p2<<")= "<<RSA::Phi(p1,p2)<<endl;
        cout<<"\nphi()= "<<mesaj.phi()<<endl;
        cout<<"\nkey= "<<key<<endl;

    *//*

        cout<<"se vor crypta nr ma mici de cit :"<<n<<endl;
       // cin>>x;
        cout<<"\nx= "<<x<<endl;
        x=RSA::RSA::crypt(5,7,33);

        cout<<"x(cryp)= "<<x<<endl;

        x=RSA::RSA::crypt(x,3,33);
        cout<<"x(cryp)= "<<x<<endl;
        //printHuman(h);



        cout<<endl<<"crypt(5,7,33)= "<<rsa::RSA::crypt(5,7,33)<<endl;
        cout<<"crypt(14,7,33)= "<<rsa::RSA::crypt(14,3,33)<<endl;





        int* ptr;
       int j=9;

        ptr=&j;



        cout<<"\nj=  "<<j<<endl;
        cout<<"ptr=&j"<<endl;
        cout<<"*ptr= "<<*ptr<<endl;
        cout<<"ptr=  "<<ptr<<endl;

        cout<<"*ptr=65  "<<endl;
        *ptr=65;
        cout<<"*ptr=  "<<*ptr<<endl;
        cout<<"j=  "<<j<<endl;
        cout<<"ptr=  "<<ptr<<endl;

    */

    long buff=0;
    buff=(long)pow(5,7)%33;

    cout<<"\nx= "<<buff<<endl;


    cout<<" crypt "<<x<<endl;
    x=(int)pow(buff,7)%33;
    cout<<x<<" decrypt "<<x<<endl;



    free(p);
        //delete h1;
    //    delete ptr;
        return 0;
}
