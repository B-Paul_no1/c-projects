#include <iostream>

using namespace std;

typedef uint8_t byte;   //0..255

byte a,PORTA=0;
bool n=false;

int main()
{
    cout << "byte = "<<sizeof(byte)<<" bytes"<<endl; //marime  byte


    // PORTA = 36
    PORTA=(1<<2)|(1<<5); //36
    cout<<"PORTA = "<<PORTA+0<<endl;

   //PORTA+=8
    PORTA|=(1<<2)|(1<<3); //44
    cout<<"PORTA = "<<PORTA+0<<endl;

    PORTA=(1<<2)|(1<<3)|(1<<4); //36
    cout<<"PORTA = "<<PORTA+0<<endl;


    cout<<"PORTA.2 = "<<PORTA+0<<endl;



//Указатель на объект - и есть Хендл ;)

//А вот пример жизненный:
char* lpszTmp="This Is Amazing String\0"; //lpszTmp - и есть хендл для строки
char* testHandle2 = lpszTmp+5; //это тоже хендл :)

    asm("NOP");

    a=(1<<2)|(1<<4);

    if ( PORTA&a) cout<<"\t DA  \n";

    //Проверка бита в переменной
    if (PORTA&((1<<2)|(1<<4))) cout<<"\Tibati! \n";

    //Verificarea pe biti multipla(PORTA&(1<<4)
    if ((PORTA&(1<<3))&&(PORTA&(1<<4))&&(PORTA&(1<<2)))
        cout<<"\t>DAFA! \n";






    return (0);
}
