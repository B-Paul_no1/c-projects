/*http://playground.arduino.cc/Interfacing/CPPWindows*/
// Font used Consolas
// Manoli Pavel 2016

#ifndef LIB_H_INCLUDED
#define LIB_H_INCLUDED

#include <iostream>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>      //pt lucru cu fisierele
#include "Serial.h"     // pt lucru cu comunicarea seriala



/*  Organizarea containerului de date bData
                  0    1-76  77  78  79
        bData[]={SOH, [...], CR, LF, EOT}
*/
//Macrouri:
#define arrSize(x) (sizeof(x)/sizeof(x[0])) //lungimea vectorului
#define asciiFiltre(x) (x<33)?asciiSpecial[x]:x
//convert int to string
#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()



#define cmd_Grn system("color a")
#define cmd_CLR system("CLS")
#define SOH 0x01				//Start Of Heading
#define SOT 0x02				//Start Of Text
#define ETX 0x03				//End Of Text
#define EOT 0x04				//End Of Transmission
#define ENQ 0x05				//Enquiry (cerere)
#define CR  0x0D				//Carriage Return '\n'
#define LF	0x0A				//Line Feed

// AT macro
#define AT              0x80            // 128
#define atPVD           0x81            // 129
#define atUID           0x82            // 130
#define atContactLen    0x83            // 131
#define atContacts      0x84            // 132
#define atReady         0x85            // 133
#define atExit          0x86            // 134
#define atReset         0x87            // 135



// Answers - Raspunsuri
#define ansPVD          0x90            // 144
#define ansUID          0x91            // 145
#define ansConLen       0x92            // 146
#define ansContact      0x93            // 147
#define ansReady        0x94            // 148
#define ansExit         0x95            // 149
#define ansReset        0x96            // 150



/* ###########################################################################*/
/* ############################ Global variables: ############################*/
/* ###########################################################################*/

    // byte UID[4]={0};                     // UID client/card
    byte UID[4]={0xFF,0xFF,0xFF,0xFF};   // UID client/card
    byte VID[3]={0xC1,0xD2,0xF3};        // Vendor  ID
    byte PID[3]={0x03,0x03,0x03};        // Product

    //asa radi pricola
    char asciiSpecial[33][5]={"NULL","SOH","STX","ETX","EOT","ENQ","ACK","BEL",
                            "BS","HT","LF","VT","FF","CR","SO","SI",
                            "DLE","DC1","DC2","DC3","DC4","NAK","SYN","ETB",
                             "CAN","EM","SUB","ESC","FS","GS","RS","US","SP"};


    int cLen=8;                         // contacts Lenght in bytes
    int pLen=0;                         // contact Pointer(index)
    const byte dataBuffLen=80;          // lugimea Pachetului de Date
    const byte dataContactLen=200;      // lugimea Pachetului de Date
    byte rstFlag=0;

    bool PVDok=true;
    bool UIDok=true;

    byte *Contacts= new byte[dataContactLen]{0};




using namespace std;



/* ###########################################################################*/
/* ############################ START Declaratii de functii ##################*/
/* ###########################################################################*/

void askPVD(byte*);             // Ask(cere) PID & VID packet
void askUID(byte*);             // Ask(cere) UID packet
void askContacts(byte*);        // Ask(cere) Contacts packet
void askContactLen(byte*);      // Ask(cere) ContactLen packet

// send dataPacket
inline void sendReady(Serial*);      // trimitem pe atReady dataPacket

byte dataPacketID(byte*);       // identificarea pachetului de date la intrare

void printBuff(byte*,byte,bool);
int  listenSerialPort(Serial*,byte*);
void clearDataBuff(byte*,byte);
void printArrayFor4BytesPerLine(byte *,byte);
void jsonExport(byte*,byte*,int);
/* ###########################################################################*/
/* ############################ END Declaratii de functii ####################*/
/* ###########################################################################*/




// inline byte waitDataPacket(Serial* port, byte *inBuff, byte idPacket)
// {
//     byte dpID=0;

//     while (1)
//     {
//         clearDataBuff(inBuff);
//         listenSerialPort(port,inBuff);
//         dpID=dataPacketID(inBuff);
//         if (dpID==idPacket) break;
//         sendReady(port,100);
//     }
//     return dpID;
// }



inline void sendReady(Serial* port,
                      int sleep=100)  // delay in ms 
{
    port->write(SOH);
    port->write(atReady);
    port->write(EOT);
    port->write(EOT);
    Sleep(sleep);           
}

void jsonExport(    byte    *uid,               // vector pt UID
                    int     *Contacts ,         // vector pt contacte
                    int     contactLenght=0    // Lenght vector pt contacte
                /*contacte inregistrate=nrContacts/4*/
                )
{
    ofstream jsonFile;      // output file 

    char res[5];
    string uidName="UID-";

    sprintf(res,"%X",uid[0]+0); uidName+=res;  uidName+="-";
    sprintf(res,"%X",uid[1]+0); uidName+=res;  uidName+="-";
    sprintf(res,"%X",uid[2]+0); uidName+=res;  uidName+="-";
    sprintf(res,"%X",uid[3]+0); uidName+=res;  uidName+=".json";

    // jsonFile.open("JSON-Test.json",ios_base::out);
    jsonFile.open(uidName,ios_base::out);

    jsonFile<<"{"<<endl;
    jsonFile<<"\t\"FileName\": \""<<uidName<<"\" ,"<<endl;

    jsonFile<<"\t\"CardID\":[ "
                        <<uid[0]+0<<", "
                        <<uid[1]+0<<", "
                        <<uid[2]+0<<", "
                        <<uid[3]+0<<" ],"<<endl;

    jsonFile<<hex<<uppercase<<"\t\"CardIDhex\":[ "
                        <<uid[0]+0<<", "
                        <<uid[1]+0<<", "
                        <<uid[2]+0<<", "
                        <<uid[3]+0<<" ],"<<dec<<endl;

   jsonFile<<"\t\"nrContacts\": "<<(contactLenght/4)<<","<<endl;
   jsonFile<<"\t\"Contacts\":"<<endl;
   jsonFile<<"\t["<<endl;

    if(!contactLenght || contactLenght<4)
        jsonFile<<"\t\t{ \"ID\" : "<<"null"<<" }"<<endl;


    while(contactLenght)
    {

        if (contactLenght%4==0 )
            if (contactLenght-4==0 )
            {
                jsonFile<<"\t\t{ \"ID\" : [ ";
                jsonFile<<*Contacts+0<<", "; Contacts++;
                jsonFile<<*Contacts+0<<", "; Contacts++;
                jsonFile<<*Contacts+0<<", "; Contacts++;
                jsonFile<<*Contacts+0<<" ]}"; Contacts++;
                jsonFile<<endl;
            }
            else
            {
                jsonFile<<"\t\t{ \"ID\" : [ ";
                jsonFile<<*Contacts+0<<", "; Contacts++;
                jsonFile<<*Contacts+0<<", "; Contacts++;
                jsonFile<<*Contacts+0<<", "; Contacts++;
                jsonFile<<*Contacts+0<<" ]},"; Contacts++;
                jsonFile<<endl;
            }
        contactLenght-=4;
    }
    jsonFile<<"\t]"<<endl;
    jsonFile<<"}";
    jsonFile.close();
}





void askPVD(byte *outBuff)      // Ask(cere) PID & VID packet
{
    outBuff[0] = SOH;
    outBuff[1] = atPVD;
    outBuff[2] = EOT;
    outBuff[3] = EOT;
}

void askUID(byte *outBuff)      // Ask(cere) UID packet
{
    outBuff[0] = SOH;
    outBuff[1] = atUID;
    outBuff[2] = EOT;
    outBuff[3] = EOT;
}

void askContacts(byte *outBuff) // Ask(cere) Contacts packet
{
    outBuff[0] = SOH;
    outBuff[1] = atContacts;
    outBuff[2] = EOT;
    outBuff[3] = EOT;
}

void askContactLen(byte *outBuff) // Ask(cere) ContactLen packet
{
    outBuff[0] = SOH;
    outBuff[1] = atContactLen;
    outBuff[2] = EOT;
    outBuff[3] = EOT;
}


// byte bData[80];      //tamponul de date pt transmitere/primire


void clearDataBuff(byte *inBuff,byte buffLen=80)
{
    while (buffLen)
    {
        buffLen--;
        inBuff[buffLen]=0x00;
    }
};


void clearDataBuff(int *inBuff,int buffLen=80)
{
    while (buffLen)
    {
        buffLen--;
        inBuff[buffLen]=0x00;
    }
};

//vedem ce a venit in SerialPort
int listenSerialPort(Serial *sHandle, byte *outBuff)
{
    byte idx=0;
    byte inData;

    while (sHandle->IsConnected() )
    {
        Sleep(2);
        sHandle->readData((char*)&inData);
        if (sHandle->cbInQue()==0) break;
        outBuff[idx]=inData;
        idx++;
    }
    return idx;
}

byte dataPacketID(byte* inBuff)
{
    byte pID=0;


    // verify PVD
    if (    inBuff[0] == SOH    && inBuff[1] == atPVD  &&
            inBuff[2] == PID[0] && inBuff[3] == PID[1] &&
            inBuff[4] == PID[2] && inBuff[5] == VID[0] &&
            inBuff[6] == VID[1] && inBuff[7] == VID[2] &&
            inBuff[8] == EOT)
        {
            cout<<"\n >>Vrea PID&VID"<<endl;
            pID=ansPVD;
        }
    // read UID
    if(
        inBuff[0] == SOH && inBuff[1] == atUID &&
       // inBuff[2] != 0   && inBuff[3] != 0 &&
       // inBuff[4] != 0   && inBuff[5] != 0 &&
        inBuff[6] == EOT )
    {
//        cout<<"\n UID "<<endl;
        UID[0]=inBuff[2];
        UID[1]=inBuff[3];
        UID[2]=inBuff[4];
        UID[3]=inBuff[5];
        pID=ansUID;
    }

    // read contact lenghts
    if (
            inBuff[0] == SOH && inBuff[1] == atContactLen &&
            inBuff[4] == EOT
        )
    {
        // cout<<"Vrea cLen"<<endl;
        cLen=inBuff[2]; cLen<<=8;   //high byte
        cLen+=inBuff[3];            // low byte
        pID=ansConLen;
    }

    // ans Ready
    if (    inBuff[0] == SOH && inBuff[1] == atReady &&
            inBuff[2] == EOT && inBuff[3] == EOT )
    {
        cout<<"\nVrea Ready"<< endl;
        pID=ansReady;
    }

    // ans atExit
    if(
        inBuff[0] == SOH && inBuff[1] == atExit &&
        inBuff[2] == EOT && inBuff[3] == EOT)
    {
        cout<<"\nVra Exit"<<endl;
        pID=ansExit;
    }

    // read contacts

    if (
            inBuff[0] == SOH && inBuff[1] == atContacts
            //
//            inBuff[3] == cPTR &&
//            inBuff[79] == EOT &&
            && (cLen <=800&& cLen!=0)
        )
        {
            // cout<<"\nvrea contacte";
            pLen=inBuff[2];
            pID=ansContact;
        }


    // ans reset
    if (    
            inBuff[0] == SOH &&     
            inBuff[1] == atReset &&     
            inBuff[3] == EOT
        )
    {
        rstFlag=(inBuff[2])?0xFF:0x00;
        pID=ansReset;
    }
        

    return pID;
}





void printArrayFor4BytesPerLine(byte *inBuff,byte bufLen=80)
{
    cout<<hex<<uppercase;
    for (int i=0; i<bufLen;i++)
    {
        if(i%4==0)
        {
            if (i)
            {
                cout<<dec<<"\t"<<i/4<<hex<<uppercase<<endl; // delimitam cite 4octeti pe linie
            }


        }


        if (inBuff[i]<0x10)
            cout<<"0"<<inBuff[i]+0<<" ";
        else
            cout<<inBuff[i]+0<<" ";

        if ( i == bufLen-1)
        {
            cout<<dec<<"\t"<<bufLen/4<<hex<<uppercase<<endl; // delimitam cite 4octeti pe linie
        }
    }
    cout<<endl;
}

//overload
void printArrayFor4BytesPerLine(int *inBuff,int bufLen=80)
{
    cout<<hex<<uppercase;
    for (int i=0; i<bufLen;i++)
    {
        if(i%4==0)
        {
            if (i)
            {
                cout<<dec<<"\t"<<i/4<<hex<<uppercase<<endl; // delimitam cite 4octeti pe linie
            }


        }


        if (inBuff[i]<0x10)
            cout<<"0"<<inBuff[i]+0<<" ";
        else
            cout<<inBuff[i]+0<<" ";

        if ( i == bufLen-1)
        {
            cout<<dec<<"\t"<<bufLen/4<<hex<<uppercase<<endl; // delimitam cite 4octeti pe linie
        }
    }
    cout<<endl;
}




void printBuff(byte *inBuff,byte buffLen=80,bool hexOut=false)
{
    int i=0;
    while (buffLen)
    {
        if(hexOut)
        {
            cout<<hex<<uppercase;
            cout<<inBuff[i]+0<<" ";
            cout<<dec;
            i++;
            buffLen--;
        }
        else
        {

            cout<<inBuff[i];
            i++;
            buffLen--;
        }

    }
    cout<<endl;
};

//convert String to Char*
inline char* strToChar(string str)
{
     char *ch;
     ch=(char*)malloc(str.size()+1);
     memcpy(ch,str.c_str(),str.size()+1);
    return ch;
}


#endif // LIB_H_INCLUDED

//Device signature = 0x1e950f
