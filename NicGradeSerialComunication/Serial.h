#ifndef SERIAL_H_INCLUDED
#define SERIAL_H_INCLUDED

//#define MCU_Wait_Time 2000 // wait time 2s

static char cPorts[17][10]={"", "\\\\.\\COM1","\\\\.\\COM2","\\\\.\\COM3",
                                "\\\\.\\COM4","\\\\.\\COM5","\\\\.\\COM6",
                                "\\\\.\\COM7","\\\\.\\COM8","\\\\.\\COM9",
                                "\\\\.\\COM10","\\\\.\\COM11","\\\\.\\COM12",
                                "\\\\.\\COM13","\\\\.\\COM14","\\\\.\\COM15"};

/*Public data structures

  CONFIG
  FLOWCONTROL
  Messages and Macros
  SerialPort_GetPortNames
  SerialPort_SetConfigurations
  SerialPort_GetConfigurations
  SerialPort_SetReadTimeout
  SerialPort_GetReadTimeout
  SerialPort_SetWriteTimeout
  SerialPort_GetWriteTimeout
  SerialPort_GetCTS
  SerialPort_GetDSR
  SerialPort_BytesToRead
  SerialPort_BytesToWrite
  SerialPort_WriteBytes
  SerialPort_ReadBytes
  SerialPort_WriteString
  SerialPort_ReadString
  SerialPort_Close
  SerialPort_Open
  SerialPort_FlushReadBuf
  SerialPort_FlushWriteBuf
  SerialPort_SetReceiveByteThreshold
  SerialPort_GetReceiveByteThreshold
  Notifications
  NMSERIAL
  SPN_DATARECEIVED
  SPN_PINCHANGED
  SPN_ERRORRECEIVED
*/
#include <windows.h>
#include <stdio.h>



class Serial
	{
	private:

		// Hander - manipultor,trener(обработчик)
		/* Handle - (pointer)valoare de referință abstractă la o resursă,
			de multe ori de memorie sau un fișier deschis, sau  PIPE*/
	    HANDLE hSerial;		//Descriptor pt citirea fluxului din COM Port
	    bool connected;		//starea conexiunii
	    COMSTAT comStatus;	//structura starii actuale ale comportului
	    DWORD errors;		  //retine ultima eroare
      unsigned long waitTime=2000;

	public:

  int comINFO;
  //constructor
  Serial(char *portName);
  //destructor
  ~Serial();
  int cbInQue(); //biti ce stau la coada pt a fi cititi

  //metode:
  int  readData(char *buffer,
                unsigned int nbChar); //return octetii disponibili, altfel -1
  bool writeData(char *buffer, unsigned int nbChar);
  bool writeData(int *buffer, unsigned int nbChar);

  bool IsConnected();
  bool IsAvailablePort(char *portName);
  char readByte();

  bool Print( char*,bool,bool );
  bool WriteABuffer(char*, DWORD );
  bool Write_Buffer(char*, DWORD );

  int close();
  int flush();
  inline bool setWaitTime(unsigned long);
  int write(int );              // transmit one byte


  //de facut:
  char* GetPortNames(){}; //Gets array of serialportames for the current PC.
  bool getDSR();
  bool getCTS();


  static void oPula(){printf("o pula\n");}


  inline unsigned long getWaitTime();
  // feature
  // ovwrload Serial::print(), transmit char , string


	};
//#############################################################
//#include "Serial.cpp"

inline unsigned long Serial::getWaitTime()
{
  return this->waitTime;
}


char Serial::readByte()
{
	char receiveData;
	if(this->readData(&receiveData,1))
		return receiveData;
	return 0;
}

int Serial::cbInQue()
{
    return this->comStatus.cbInQue;
}

//consturctor: Initializierea conexiunii
Serial::Serial(char *portName)
{
	DCB dcbSerialParam;		//structura pt initializierea comuna a potrtului


  this->connected=false;
  this->hSerial= CreateFile(portName,                 	  //nume fisier
                            GENERIC_READ|GENERIC_WRITE,   //accese la fisier citire/inscriere
                            0,							              // portul nu e disponibil public(shared)
                            NULL,							            //descriptorul portului nu se mosteneste, este implicit protejat
                            OPEN_EXISTING,                //portul trebuie sa se dechida ca fisier existent
                            FILE_ATTRIBUTE_NORMAL,        //flag de atribuire la uz
                            NULL);                        //pointer la fisier de sablon

  if (this->hSerial==INVALID_HANDLE_VALUE)
  {	  //nu sa reusit sa deschidem portul
      if(GetLastError()==ERROR_FILE_NOT_FOUND)
      {
          printf("Error: Handle was not attached, Reason:%s not avaible\n", portName);
      }
      else
      {
          printf("Error!!!");
      }
  }
  else
  {
  	/*prima proprietate a dcbSerialParam ,
  	se va utiliza pt controlul corerectii functionarii [...]*/
  	dcbSerialParam.DCBlength=sizeof(DCB);

    /* Set Timeouts(ms) */
    COMMTIMEOUTS timeouts={0};

    timeouts.ReadIntervalTimeout=         50;
    timeouts.ReadTotalTimeoutConstant=    50;
    timeouts.ReadTotalTimeoutMultiplier=  10;
    timeouts.WriteTotalTimeoutConstant=   50;
    timeouts.WriteTotalTimeoutMultiplier= 10;

    if(!SetCommTimeouts(this->hSerial, &timeouts))
    {
     /* Error occurred. Inform user */
      printf("Error!\n");
    }
      // citim structura dcbSerialParam din port
      if (!GetCommState(this->hSerial, &dcbSerialParam))
      {
          printf("failed to get current serial parameters!");

      }
      else
      {
      // dcbSerialParam.BaudRate=CBR_9600;
        dcbSerialParam.BaudRate=9600;		//viteza de comunicare in BAUD
        dcbSerialParam.ByteSize=8;			//marimea pachetului de date in biti
        dcbSerialParam.StopBits=ONESTOPBIT;	//nr bitiilor de STOP
        dcbSerialParam.Parity=  NOPARITY;		//verificarea la bitii de paritate

        dcbSerialParam.fBinary=        true;	//pornim regimul binar de schimb
        dcbSerialParam.fOutxCtsFlow=   false;	// oprim modul de urmarire a semnalului CTS
        dcbSerialParam.fOutxDsrFlow=   false;	// oprim modul de urmarire a semnalului DSR
        dcbSerialParam.fDsrSensitivity=false; // sensibilitatea driverului fata de starea linii DSR
        dcbSerialParam.fNull=          false;	//permisiunea de a primi octeti nuli
        dcbSerialParam.fRtsControl=RTS_CONTROL_DISABLE; //oprim uzul linii RTS
        dcbSerialParam.fAbortOnError=  false;  //interzicem oprirea operatiil RW la eroare

      // dcbSerialParam.DCBlength
        dcbSerialParam.fDtrControl=DTR_CONTROL_ENABLE; // permitem utilizarea linii DTR

         // Setam parametrii si ii verificam la carectitudine
         if (!SetCommState(hSerial, &dcbSerialParam))
         {
              printf("Alert: Could not SerialPort parameters");
         }
         else
         {
             this->connected=true;

             // Curatim memoriile tampon pt RX si TX
             PurgeComm(this->hSerial,
                       PURGE_RXCLEAR|PURGE_TXCLEAR); // sterge memoria de tampon la primire si transmitere
             // Sleep(MCU_Wait_Time);
             Sleep(this->waitTime);
         }
      }
  }
}
//destructor: Close the connection
Serial::~Serial()
{
  if(this->connected)
  {
      this->connected=false;
      CloseHandle(this->hSerial); //inchidem coneziunea seriala
  }
}

int Serial::readData(char *buffer, unsigned int nbChar=1)
{
    DWORD bytesRead;		//nr de octeti cititi
    unsigned int toRead;	// nr de octeti ceruti pt citire
    // funtie pt iterogarea starii Serial Port
    ClearCommError(this->hSerial, &this->errors, &this->comStatus);

    // verificam daca exista cv de citit
   //	cbInQue - octeti ce stau la coada pt a fi citi
    if (this->comStatus.cbInQue>0)
    {
    	/*verificam daca este indeajuns nr octetiilor pt citere ,
    	  in caz contrar vom citi nr disponibili de octeti pt perveni
    	  blocarea programului */
        if(this->comStatus.cbInQue>nbChar)
        {
            toRead=nbChar; 			//pt citire
        }
        else
        {
            toRead=this->comStatus.cbInQue; 	//disponibili din status
        }

        if (ReadFile(this->hSerial,buffer,toRead, &bytesRead, NULL))
        {
          PurgeComm( this->hSerial,
            PURGE_RXABORT|PURGE_TXABORT
            // |PURGE_RXCLEAR|PURGE_TXCLEAR
            );
            return bytesRead;
        }
    }
    return 0;
}

bool Serial::writeData(char *buffer,unsigned int nbChar)
{
    DWORD bytesSend;
    if (!WriteFile(this->hSerial,   //port
        (void*)buffer,              //data ce dorim so trimitem
        nbChar,                     // de octeti ce dorim sa trimitem
        &bytesSend,
        0))
    {
        ClearCommError(this->hSerial,&this->errors,&this->comStatus);
        return false;
    }
    else
    {
        return true;
    }
}



bool Serial::writeData(int *buffer,unsigned int nbChar)
{
    DWORD bytesSend;
    if (!WriteFile(this->hSerial,   //port
        (void*)buffer,              //data ce dorim so trimitem
        nbChar,                     // de octeti ce dorim sa trimitem
        &bytesSend,
        0))
    {
        ClearCommError(this->hSerial,&this->errors,&this->comStatus);
        return false;
    }
    else
    {
        return true;
    }
}

 // transmit one byte
int Serial::write(int a )
{
  return Serial::writeData(&a,1);
  // return 0;
}              //

//overload writeData
bool Serial::Print( char *buffer,           //Transmit data
                        bool CR_LF=true,    //Carriage Return si New Line
                        bool null=true)    // hz
{

  //aflam lungimea bufferului
  while ((*buffer)!='\0')
  {
    if(this->IsConnected())
    {
      this->writeData(buffer,1);
    }
    buffer++;

  }
  if (CR_LF)
  {
    if(this->IsConnected())
    {
      this->writeData("\n",1);
    }
  }
  return true;
}

bool Serial::WriteABuffer(char * lpBuf, DWORD dwToWrite)
{
  OVERLAPPED osWrite = {0};
  DWORD dwWritten;
  DWORD dwRes;
  BOOL fRes;

  // Create this write operation's OVERLAPPED structure's hEvent.
  osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
  if (osWrite.hEvent == NULL)
      // error creating overlapped event handle
      return FALSE;

   // Issue write.
  if (!WriteFile(this->hSerial, lpBuf, dwToWrite, &dwWritten, &osWrite)) {
    if (GetLastError() != ERROR_IO_PENDING)
    {
       // WriteFile failed, but isn't delayed. Report error and abort.
       fRes = FALSE;
    }
    else
       // Write is pending.
       dwRes = WaitForSingleObject(osWrite.hEvent, INFINITE);

  switch(dwRes)
         {
            // OVERLAPPED structure's event has been signaled.
            case WAIT_OBJECT_0:
                 if (!GetOverlappedResult(this->hSerial, &osWrite, &dwWritten, FALSE))
                       fRes = FALSE;
                 else
                  // Write operation completed successfully.
                  fRes = TRUE; break;

            default:
                 // An error has occurred in WaitForSingleObject.
                 // This usually indicates a problem with the
                // OVERLAPPED structure's event handle.
                 fRes = FALSE; break;
         }
    }
  else
      // WriteFile completed immediately.
     fRes = TRUE;
 // PurgeComm( this->hSerial,
 //            PURGE_RXABORT|PURGE_TXABORT|
 //            PURGE_RXCLEAR|PURGE_TXCLEAR);

  CloseHandle(osWrite.hEvent);

  return fRes;
}


bool Serial::Write_Buffer(char * lpBuf, DWORD dwToWrite)
{
   OVERLAPPED osWrite = {0};
   DWORD dwWritten;
   BOOL fRes;

   // Create this writes OVERLAPPED structure hEvent.
   osWrite.hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
   if (osWrite.hEvent == NULL)
      // Error creating overlapped event handle.
      return FALSE;

   // Issue write.
   if (!WriteFile(this->hSerial, lpBuf, dwToWrite, &dwWritten, &osWrite)) {
      if (GetLastError() != ERROR_IO_PENDING) {
         // WriteFile failed, but it isn't delayed. Report error and abort.
         fRes = FALSE;
      }
      else {
         // Write is pending.
         if (!GetOverlappedResult(this->hSerial, &osWrite, &dwWritten, TRUE))
            fRes = FALSE;
         else
            // Write operation completed successfully.
            fRes = TRUE;
      }
   }
   else
      // WriteFile completed immediately.
      fRes = TRUE;
    // PurgeComm( this->hSerial,
    //         PURGE_RXABORT|PURGE_TXABORT|
    //         PURGE_RXCLEAR|PURGE_TXCLEAR);

   CloseHandle(osWrite.hEvent);
   return fRes;
}

int Serial::flush()
{
    FlushFileBuffers(this->hSerial);
  return 0;
}


int Serial::close()
{
  CloseHandle(this->hSerial);
  return 0;
}

bool Serial::IsConnected()
{
    return this->connected;
}

#endif // SERIAL_H_INCLUDED






















/*
  #include "serial-port.h"
  #include <Windows.h>

  // MSDN
  // CreateFile > Remarks > Communications Resources
  //   To specify a COM port number greater than 9, use the following syntax: "\\.\COM10".

  void* serial_port_open(const char *name)
  {
    HANDLE h;
    h = CreateFileA(name,
      GENERIC_READ | GENERIC_WRITE,
      0,        // must be opened with exclusive-access
      NULL,     // default security attributes
      OPEN_EXISTING,  // must use OPEN_EXISTING
      0,        // not overlapped I/O
      NULL);      // hTemplate must be NULL for comm devices

    if(INVALID_HANDLE_VALUE == h)
      return 0;

    return (void*)h;
  }

  int serial_port_close(void* port)
  {
    HANDLE h = (HANDLE)port;
    CloseHandle(h);
    return 0;
  }

  int serial_port_flush(void* port)
  {
    HANDLE h = (HANDLE)port;
    FlushFileBuffers(h);
    return 0;
  }

  static int serial_port_baud_rate(int baudrate)
  {
    switch(baudrate)
    {
    case 110: return CBR_110;
    case 300: return CBR_300;
    case 600: return CBR_600;
    case 1200: return CBR_1200;
    case 2400: return CBR_2400;
    case 4800: return CBR_4800;
    case 9600: return CBR_9600;
    case 14400: return CBR_14400;
    case 19200: return CBR_19200;
    case 56000: return CBR_56000;
    case 57600: return CBR_57600;
    case 115200: return CBR_115200;
    case 128000: return CBR_128000;
    case 256000: return CBR_256000;
    default: return -1;
    }
  }

  int serial_port_setattr(void* port, int baudrate, int databits, int parity, int stopbits, int flowctrl)
  {
    DCB dcb;
    HANDLE h = (HANDLE)port;

    ZeroMemory(&dcb, sizeof(dcb));
    dcb.DCBlength = sizeof(dcb);
    GetCommState(h, &dcb);

    dcb.fBinary = TRUE;

    dcb.BaudRate = serial_port_baud_rate(baudrate);
    if(-1 == dcb.BaudRate)
      return -1;

    switch(parity)
    {
    case SERIAL_PARITY_NONE:
      dcb.Parity = NOPARITY;
      break;
    case SERIAL_PARITY_ODD:
      dcb.Parity = ODDPARITY;
      break;
    case SERIAL_PARITY_EVEN:
      dcb.Parity = EVENPARITY;
      break;
    case SERIAL_PARITY_MARK:
      dcb.Parity = MARKPARITY;
      break;
    case SERIAL_PARITY_SPACE:
      dcb.Parity = SPACEPARITY;
      break;
    default:
      return -1;
    }

    switch(stopbits)
    {
    case SERIAL_STOPBITS_10:
      dcb.StopBits = ONESTOPBIT;
      break;
    case SERIAL_STOPBITS_15:
      dcb.StopBits = ONE5STOPBITS;
      break;
    case SERIAL_STOPBITS_20:
      dcb.StopBits = TWOSTOPBITS;
      break;
    default:
      return -1;
    }

    switch(databits)
    {
    case SERIAL_DATABITS_5:
      dcb.ByteSize = 5;
      break;
    case SERIAL_DATABITS_6:
      dcb.ByteSize = 6;
      break;
    case SERIAL_DATABITS_7:
      dcb.ByteSize = 7;
      break;
    case SERIAL_DATABITS_8:
      dcb.ByteSize = 8;
      break;
    case SERIAL_DATABITS_16:
      dcb.ByteSize = 16;
      break;
    default:
      return -1;
    }

    // MSDN > BuildCommDCB > Remarks
    switch(flowctrl)
    {
      // Hardware flow control uses the RTS/CTS (or DTR/DSR) signals to communicate.
    case SERIAL_FLOWCTRL_HARDWARE:
      dcb.fInX = FALSE;
      dcb.fOutX = FALSE;
      dcb.fOutxCtsFlow = TRUE;
      dcb.fOutxDsrFlow = TRUE;
      dcb.fDtrControl = DTR_CONTROL_HANDSHAKE;
      dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
      break;

    case SERIAL_FLOWCTRL_SOFTWARE:
      dcb.fInX = TRUE;
      dcb.fOutX = TRUE;
      dcb.fOutxCtsFlow = FALSE;
      dcb.fOutxDsrFlow = FALSE;
      dcb.fDtrControl = DTR_CONTROL_ENABLE;
      dcb.fRtsControl = RTS_CONTROL_ENABLE;
      break;

    case SERIAL_FLOWCTRL_DISABLE:
      dcb.fInX = FALSE;
      dcb.fOutX = FALSE;
      dcb.fOutxCtsFlow = FALSE;
      dcb.fOutxDsrFlow = FALSE;
      dcb.fDtrControl = DTR_CONTROL_ENABLE;
      dcb.fRtsControl = RTS_CONTROL_ENABLE;

    default:
      return -1;
    }

    if(!SetCommState(h, &dcb))
      return -1;
    return 0;
  }

  int serial_port_write(void* port, const void* data, int bytes)
  {
    DWORD n = 0;
    HANDLE h = (HANDLE)port;

    if(!WriteFile(h, data, bytes, &n, NULL))
      return -1;

    return n;
  }

  int serial_port_read(void* port, void* data, int bytes)
  {
    DWORD n = 0;
    HANDLE h = (HANDLE)port;

    if(!ReadFile(h, data, bytes, &n, NULL))
      return -1;

    return n;
  }

*/




