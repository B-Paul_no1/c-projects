#include "LIB.h"        // toate bibliotecii sunt incluse acolo
using namespace std;

#define atReadySend() asm("nop");

void askUID(Serial *hSerial)
{
    hSerial->write(SOH);
    hSerial->write(atUID);
    hSerial->write(EOT);
    hSerial->write(EOT);
}


void parseContacts(byte *inBuff,byte *outBuff, byte bufLen=76)
{

  for (int i=0; i<bufLen;i++)
  {
      outBuff[i]=inBuff[i+3];
  }

}

inline void waitDataPacket(Serial *port, byte *inBuf, byte idPacket)
{
    byte inData=0;
    while(1)    // wait for response  (atReset)
    {
        clearDataBuff(inBuf);
        listenSerialPort(port,inBuf);
        inData=dataPacketID(inBuf);

        if (inData==idPacket)
        {
    //        cout<<">> Vrea atReset"<<endl;
    //        cout<<">> rstFlag = "<< rstFlag+0<<endl;
            break;
        }
        sendReady(port,100);
    }


}


int main()
{
    system("color 0a"); // green char, black background

    cout<<endl;

    short portNum=5;

    cout<<"Please enter port Number ex COM5 enter 5"<<endl;
    cin>>portNum;


    Serial *comPort=new Serial(cPorts[portNum]);


    if (!comPort->IsConnected())
    {
        cout<<"\t Zaibisi, nui legatura"<<endl;
        comPort->close();
        exit(1);

    }

    byte inData;        //received Data
    byte inputData[dataBuffLen]={0};
    byte inOK = true;

    // wait UID dataPacket
    while(1)
    {
        clearDataBuff(inputData);
        listenSerialPort(comPort,inputData);
//        printBuff(inputData,80,true);
        sendReady(comPort,100);
        if(dataPacketID(inputData)==ansUID)
            break;
    }
    // tiparim
    cout<<"UID:"<<endl;
    printBuff(UID,4,true);

    // wait cLen
    while(inOK)
    {
       listenSerialPort(comPort,inputData);
       inData=dataPacketID(inputData);
       clearDataBuff(inputData);
       if (inData == ansConLen) // daca sa depistat clen esim din
       {
           inOK=false;
//               cout<<"Clen primit"<<endl;
           continue;
       }
       // trimitem atReady
        comPort->write(SOH);
        comPort->write(atReady);
        comPort->write(EOT);
        comPort->write(EOT);
        Sleep(100);
    }
        inOK=true;


        if (cLen>800 )  // 200 contacts
        {
            cout<<"ContactLength= "<<cLen<<endl;
            cout<<"Overload Contact length"<<endl;
            comPort->close();
            exit(0);
        }
        cout<<"cLen= "<<cLen<<endl;

        byte *ARR=new byte[80]; // temporary buff
        int *temp=new int[800]; // temp buf for all contacts
        int cPtr=0;

        clearDataBuff(ARR,80);
        clearDataBuff(temp,cLen);
//        printArrayFor4BytesPerLine(ARR, cLen);

        byte dp=0; // cite pachete de date trebuie transmise pt cLen
        byte p=1;  // packet nr


        dp=cLen/76;
        dp+=(cLen%76)?1:0;


//        if (dp&&0)
        if (dp)
        {
            cout<<"\n iteratii: "<<dp+0<<endl;

            while(dp)
            {
                // wait next data packet
                while(1)
                {
                   clearDataBuff(inputData);
                   listenSerialPort(comPort,inputData);  // pasrse data
                   inData=dataPacketID(inputData);

                   if (inData == ansContact)
                   {
                       clearDataBuff(ARR);
                       parseContacts(inputData,ARR);


                       for(int i=0; i<pLen; i++)
                       {
                           temp[i+cPtr]=inputData[i+3];
                       }
                       cPtr+=pLen;;
                       break;

                   }
                   // trimitem atReady

                    sendReady(comPort,100);

                }
                Sleep(100);

                cout<<"packet: "<<dec<<p+0<<endl; p++;
                cout<<"ptr: "<<cPtr+0<<endl;
                cout<<"Packet Lenght pLen="<<pLen<<endl;

                printArrayFor4BytesPerLine(ARR,pLen);

                dp--;

            }/////// end while (dp)

           // wait for response  (atReset)
            waitDataPacket(comPort,inputData,ansReset);
            Sleep(10);


        }   ///// end if(dp)

        else  // send atExit
        {
            cout<<"\n Nus contacte"<<endl;
            // send atExit
            comPort->write(SOH);    comPort->write(atExit);
            comPort->write(EOT);    comPort->write(EOT);
            Sleep(10);              clearDataBuff(inputData);
        }



        clearDataBuff(inputData);
//        Sleep(500);


    cout<<endl;
    cout<<endl;
    cout<<"temp: "<<endl;
//    system("pause");

    printArrayFor4BytesPerLine(temp,cLen);

    jsonExport(UID,temp,cLen); //la testari nu trebuie


    comPort->close();


    delete[] Contacts;
    delete[] ARR;
    delete[] temp;

    return 0;       // succes return
}
/******************************************************************************/
/**************************End main()******************************************/
/******************************************************************************/





