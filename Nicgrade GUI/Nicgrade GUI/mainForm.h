#pragma once
#include <cstdint>

namespace NicgradeGUI 
{

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO::Ports;

	typedef uint8_t byte;

	

public ref class mainForm : public System::Windows::Forms::Form
{
	private: int cLen = 0;
private: System::Windows::Forms::Timer^  timer1;
protected: String^ rxData;

	

	

/*################ Constructor ################*/
	public:	mainForm(void)
			{
				InitializeComponent();
				//this->textBox_cLen->Text = Convert::ToString(cLen);
				//comboBox1->Text = t
				comboBox1->Items->Clear();
				fiindComPorts();
				// my Methodes:
				
			
			}



	
		
			/*################ Destructor ################*/
	protected:~mainForm()
		{
			if (components)
			{
				delete components;
			}
		}
	// Crearea obiectelor:
	private: System::Windows::Forms::Button^	button1;
	private: System::Windows::Forms::Label^		label_myUID;
	private: System::Windows::Forms::TextBox^	textBox_MyUID;
	private: System::Windows::Forms::TextBox^	textBox_cLen;
	private: System::Windows::Forms::Label^		label_cLen;
	private: System::Windows::Forms::Label^		label_ContactRecord;
	private: System::Windows::Forms::ListBox^	listBox_ContactRecord;
	private: System::Windows::Forms::Button^	button2;
	private: System::Windows::Forms::Label^		label1;
	private: System::Windows::Forms::ComboBox^  comboBox1;
	private: System::Windows::Forms::ComboBox^  comboBox2;
	private: System::Windows::Forms::Label^		label2;
	private: System::Windows::Forms::ComboBox^  comboBox3;
	private: System::Windows::Forms::Label^		label3;
	private: System::Windows::Forms::ComboBox^  comboBox4;
	private: System::Windows::Forms::ComboBox^  comboBox5;
	private: System::Windows::Forms::Button^	button3;
	private: System::Windows::Forms::NotifyIcon^  notifyIcon1;
	private: System::Windows::Forms::ToolTip^	toolTip_Info;
	private: System::Windows::Forms::Label^		label4;
	private: System::Windows::Forms::Label^		label5;
	private: System::Windows::Forms::ToolStripPanel^  BottomToolStripPanel;
	private: System::Windows::Forms::ToolStripPanel^  TopToolStripPanel;
	private: System::Windows::Forms::ToolStripPanel^  RightToolStripPanel;
	private: System::Windows::Forms::ToolStripPanel^  LeftToolStripPanel;
	private: System::Windows::Forms::ToolStripContentPanel^  ContentPanel;
	private: System::Windows::Forms::GroupBox^  groupBox_hide;
	private: System::Windows::Forms::Button^  button_connect;
private: System::Windows::Forms::Button^  button_disconnect;

	private: System::IO::Ports::SerialPort^  serialPort1;
private: System::Windows::Forms::Button^  button_rescan;

private: System::Windows::Forms::Label^  label6;
private: System::Windows::Forms::StatusStrip^  statusStrip1;
private: System::Windows::Forms::ToolStripStatusLabel^  toolStripStatusLabel_comPort;
private: System::Windows::Forms::ToolStripStatusLabel^  toolStripStatusLabel_baudRate;
private: System::Windows::Forms::ToolStripProgressBar^  toolStripProgressBar_comSatus;

private: System::Windows::Forms::TextBox^  textBox2;
private: System::Windows::Forms::Label^  label7;
private: System::Windows::Forms::Label^  label8;

private: System::ComponentModel::IContainer^  components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
	void InitializeComponent(void)
		{
		this->components = (gcnew System::ComponentModel::Container());
		System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(mainForm::typeid));
		this->button1 = (gcnew System::Windows::Forms::Button());
		this->label_myUID = (gcnew System::Windows::Forms::Label());
		this->textBox_MyUID = (gcnew System::Windows::Forms::TextBox());
		this->textBox_cLen = (gcnew System::Windows::Forms::TextBox());
		this->label_cLen = (gcnew System::Windows::Forms::Label());
		this->label_ContactRecord = (gcnew System::Windows::Forms::Label());
		this->listBox_ContactRecord = (gcnew System::Windows::Forms::ListBox());
		this->button2 = (gcnew System::Windows::Forms::Button());
		this->label1 = (gcnew System::Windows::Forms::Label());
		this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
		this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
		this->label2 = (gcnew System::Windows::Forms::Label());
		this->comboBox3 = (gcnew System::Windows::Forms::ComboBox());
		this->label3 = (gcnew System::Windows::Forms::Label());
		this->comboBox4 = (gcnew System::Windows::Forms::ComboBox());
		this->comboBox5 = (gcnew System::Windows::Forms::ComboBox());
		this->button3 = (gcnew System::Windows::Forms::Button());
		this->notifyIcon1 = (gcnew System::Windows::Forms::NotifyIcon(this->components));
		this->toolTip_Info = (gcnew System::Windows::Forms::ToolTip(this->components));
		this->button_connect = (gcnew System::Windows::Forms::Button());
		this->button_disconnect = (gcnew System::Windows::Forms::Button());
		this->button_rescan = (gcnew System::Windows::Forms::Button());
		this->label4 = (gcnew System::Windows::Forms::Label());
		this->label5 = (gcnew System::Windows::Forms::Label());
		this->BottomToolStripPanel = (gcnew System::Windows::Forms::ToolStripPanel());
		this->TopToolStripPanel = (gcnew System::Windows::Forms::ToolStripPanel());
		this->RightToolStripPanel = (gcnew System::Windows::Forms::ToolStripPanel());
		this->LeftToolStripPanel = (gcnew System::Windows::Forms::ToolStripPanel());
		this->ContentPanel = (gcnew System::Windows::Forms::ToolStripContentPanel());
		this->groupBox_hide = (gcnew System::Windows::Forms::GroupBox());
		this->serialPort1 = (gcnew System::IO::Ports::SerialPort(this->components));
		this->label6 = (gcnew System::Windows::Forms::Label());
		this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
		this->toolStripStatusLabel_comPort = (gcnew System::Windows::Forms::ToolStripStatusLabel());
		this->toolStripStatusLabel_baudRate = (gcnew System::Windows::Forms::ToolStripStatusLabel());
		this->toolStripProgressBar_comSatus = (gcnew System::Windows::Forms::ToolStripProgressBar());
		this->textBox2 = (gcnew System::Windows::Forms::TextBox());
		this->label7 = (gcnew System::Windows::Forms::Label());
		this->label8 = (gcnew System::Windows::Forms::Label());
		this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
		this->groupBox_hide->SuspendLayout();
		this->statusStrip1->SuspendLayout();
		this->SuspendLayout();
		// 
		// button1
		// 
		this->button1->CausesValidation = false;
		this->button1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->button1->Location = System::Drawing::Point(12, 292);
		this->button1->Name = L"button1";
		this->button1->Size = System::Drawing::Size(75, 23);
		this->button1->TabIndex = 0;
		this->button1->Text = L"Save UID";
		this->button1->UseVisualStyleBackColor = true;
		// 
		// label_myUID
		// 
		this->label_myUID->AutoSize = true;
		this->label_myUID->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->label_myUID->Location = System::Drawing::Point(13, 13);
		this->label_myUID->Name = L"label_myUID";
		this->label_myUID->Size = System::Drawing::Size(55, 16);
		this->label_myUID->TabIndex = 1;
		this->label_myUID->Text = L"My UID:";
		// 
		// textBox_MyUID
		// 
		this->textBox_MyUID->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->textBox_MyUID->Location = System::Drawing::Point(74, 13);
		this->textBox_MyUID->MaxLength = 10;
		this->textBox_MyUID->Name = L"textBox_MyUID";
		this->textBox_MyUID->ReadOnly = true;
		this->textBox_MyUID->ShortcutsEnabled = false;
		this->textBox_MyUID->Size = System::Drawing::Size(100, 22);
		this->textBox_MyUID->TabIndex = 2;
		this->textBox_MyUID->Tag = L"adaf";
		this->textBox_MyUID->Text = L"A1-A2-A3-A4";
		this->textBox_MyUID->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		this->toolTip_Info->SetToolTip(this->textBox_MyUID, L"My UID Blea");
		// 
		// textBox_cLen
		// 
		this->textBox_cLen->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->textBox_cLen->Location = System::Drawing::Point(125, 41);
		this->textBox_cLen->MaxLength = 3;
		this->textBox_cLen->Name = L"textBox_cLen";
		this->textBox_cLen->ReadOnly = true;
		this->textBox_cLen->Size = System::Drawing::Size(49, 22);
		this->textBox_cLen->TabIndex = 4;
		this->textBox_cLen->Text = L"200";
		this->textBox_cLen->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
		// 
		// label_cLen
		// 
		this->label_cLen->AutoSize = true;
		this->label_cLen->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->label_cLen->Location = System::Drawing::Point(13, 41);
		this->label_cLen->Name = L"label_cLen";
		this->label_cLen->Size = System::Drawing::Size(106, 16);
		this->label_cLen->TabIndex = 3;
		this->label_cLen->Text = L"Contacts Length:";
		// 
		// label_ContactRecord
		// 
		this->label_ContactRecord->AutoSize = true;
		this->label_ContactRecord->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular,
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
		this->label_ContactRecord->Location = System::Drawing::Point(12, 71);
		this->label_ContactRecord->Name = L"label_ContactRecord";
		this->label_ContactRecord->Size = System::Drawing::Size(111, 16);
		this->label_ContactRecord->TabIndex = 5;
		this->label_ContactRecord->Text = L"Contacts Record:";
		// 
		// listBox_ContactRecord
		// 
		this->listBox_ContactRecord->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular,
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
		this->listBox_ContactRecord->FormattingEnabled = true;
		this->listBox_ContactRecord->ItemHeight = 16;
		this->listBox_ContactRecord->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"A1-A2-A3-A4", L"A1-A2-A3-A4" });
		this->listBox_ContactRecord->Location = System::Drawing::Point(16, 90);
		this->listBox_ContactRecord->MaximumSize = System::Drawing::Size(120, 196);
		this->listBox_ContactRecord->MinimumSize = System::Drawing::Size(120, 20);
		this->listBox_ContactRecord->Name = L"listBox_ContactRecord";
		this->listBox_ContactRecord->SelectionMode = System::Windows::Forms::SelectionMode::None;
		this->listBox_ContactRecord->Size = System::Drawing::Size(120, 196);
		this->listBox_ContactRecord->TabIndex = 6;
		// 
		// button2
		// 
		this->button2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->button2->Location = System::Drawing::Point(93, 292);
		this->button2->Name = L"button2";
		this->button2->Size = System::Drawing::Size(75, 23);
		this->button2->TabIndex = 7;
		this->button2->Text = L"Reset";
		this->toolTip_Info->SetToolTip(this->button2, L"Tat o sa fie bine\r\nCand tata va veni\r\nPe mama o va fute \r\nPe noi ne va pizdi");
		this->button2->UseVisualStyleBackColor = true;
		this->button2->Click += gcnew System::EventHandler(this, &mainForm::button2_Click);
		// 
		// label1
		// 
		this->label1->AutoSize = true;
		this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->label1->Location = System::Drawing::Point(6, 22);
		this->label1->Name = L"label1";
		this->label1->Size = System::Drawing::Size(68, 16);
		this->label1->TabIndex = 8;
		this->label1->Text = L"COM Port:";
		// 
		// comboBox1
		// 
		this->comboBox1->DisplayMember = L"1";
		this->comboBox1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
		this->comboBox1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->comboBox1->FormattingEnabled = true;
		this->comboBox1->Items->AddRange(gcnew cli::array< System::Object^  >(2) { L"COM 1", L"COM 2" });
		this->comboBox1->Location = System::Drawing::Point(85, 20);
		this->comboBox1->Name = L"comboBox1";
		this->comboBox1->Size = System::Drawing::Size(87, 24);
		this->comboBox1->Sorted = true;
		this->comboBox1->TabIndex = 9;
		this->toolTip_Info->SetToolTip(this->comboBox1, L"Curent COM Port");
		this->comboBox1->ValueMember = L"1";
		// 
		// comboBox2
		// 
		this->comboBox2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->comboBox2->FormattingEnabled = true;
		this->comboBox2->Items->AddRange(gcnew cli::array< System::Object^  >(6) { L"1200", L"2400", L"4800", L"9600", L"14400", L"19200" });
		this->comboBox2->Location = System::Drawing::Point(85, 50);
		this->comboBox2->Name = L"comboBox2";
		this->comboBox2->Size = System::Drawing::Size(87, 24);
		this->comboBox2->TabIndex = 11;
		this->comboBox2->Text = L"9600";
		this->comboBox2->SelectedIndexChanged += gcnew System::EventHandler(this, &mainForm::comboBox2_SelectedIndexChanged);
		// 
		// label2
		// 
		this->label2->AutoSize = true;
		this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->label2->Location = System::Drawing::Point(6, 52);
		this->label2->Name = L"label2";
		this->label2->Size = System::Drawing::Size(75, 16);
		this->label2->TabIndex = 10;
		this->label2->Text = L"Baud Rate:";
		// 
		// comboBox3
		// 
		this->comboBox3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->comboBox3->FormattingEnabled = true;
		this->comboBox3->Items->AddRange(gcnew cli::array< System::Object^  >(4) { L"5", L"6", L"7", L"8" });
		this->comboBox3->Location = System::Drawing::Point(87, 105);
		this->comboBox3->Name = L"comboBox3";
		this->comboBox3->Size = System::Drawing::Size(87, 24);
		this->comboBox3->TabIndex = 13;
		this->comboBox3->Text = L"8";
		// 
		// label3
		// 
		this->label3->AutoSize = true;
		this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->label3->Location = System::Drawing::Point(13, 108);
		this->label3->Name = L"label3";
		this->label3->Size = System::Drawing::Size(61, 16);
		this->label3->TabIndex = 12;
		this->label3->Text = L"Data bits";
		// 
		// comboBox4
		// 
		this->comboBox4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->comboBox4->FormattingEnabled = true;
		this->comboBox4->Items->AddRange(gcnew cli::array< System::Object^  >(5) { L"none", L"odd", L"even", L"mark", L"space" });
		this->comboBox4->Location = System::Drawing::Point(87, 135);
		this->comboBox4->Name = L"comboBox4";
		this->comboBox4->Size = System::Drawing::Size(87, 24);
		this->comboBox4->TabIndex = 15;
		this->comboBox4->Text = L"none";
		// 
		// comboBox5
		// 
		this->comboBox5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->comboBox5->FormattingEnabled = true;
		this->comboBox5->Items->AddRange(gcnew cli::array< System::Object^  >(3) { L"1", L"1.5", L"2" });
		this->comboBox5->Location = System::Drawing::Point(87, 165);
		this->comboBox5->Name = L"comboBox5";
		this->comboBox5->Size = System::Drawing::Size(87, 24);
		this->comboBox5->TabIndex = 17;
		this->comboBox5->Text = L"1";
		// 
		// button3
		// 
		this->button3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
		this->button3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->button3->Location = System::Drawing::Point(397, 292);
		this->button3->Name = L"button3";
		this->button3->Size = System::Drawing::Size(75, 23);
		this->button3->TabIndex = 18;
		this->button3->Text = L">>";
		this->button3->UseVisualStyleBackColor = true;
		this->button3->Click += gcnew System::EventHandler(this, &mainForm::button3_Click);
		// 
		// notifyIcon1
		// 
		this->notifyIcon1->BalloonTipIcon = System::Windows::Forms::ToolTipIcon::Info;
		this->notifyIcon1->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"notifyIcon1.Icon")));
		this->notifyIcon1->Text = L"notifyIcon1";
		this->notifyIcon1->Visible = true;
		// 
		// button_connect
		// 
		this->button_connect->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->button_connect->Location = System::Drawing::Point(292, 234);
		this->button_connect->Name = L"button_connect";
		this->button_connect->Size = System::Drawing::Size(75, 23);
		this->button_connect->TabIndex = 22;
		this->button_connect->Text = L"Connect";
		this->toolTip_Info->SetToolTip(this->button_connect, L"Tat o sa fie bine\r\nCand tata va veni\r\nPe mama o va fute \r\nPe noi ne va pizdi");
		this->button_connect->UseVisualStyleBackColor = true;
		this->button_connect->Click += gcnew System::EventHandler(this, &mainForm::button_connect_Click);
		// 
		// button_disconnect
		// 
		this->button_disconnect->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular,
			System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
		this->button_disconnect->Location = System::Drawing::Point(377, 234);
		this->button_disconnect->Name = L"button_disconnect";
		this->button_disconnect->Size = System::Drawing::Size(95, 23);
		this->button_disconnect->TabIndex = 23;
		this->button_disconnect->Text = L"Disconnect";
		this->toolTip_Info->SetToolTip(this->button_disconnect, L"Tat o sa fie bine\r\nCand tata va veni\r\nPe mama o va fute \r\nPe noi ne va pizdi");
		this->button_disconnect->UseVisualStyleBackColor = true;
		this->button_disconnect->Click += gcnew System::EventHandler(this, &mainForm::button_disconnect_Click);
		// 
		// button_rescan
		// 
		this->button_rescan->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->button_rescan->Location = System::Drawing::Point(292, 263);
		this->button_rescan->Name = L"button_rescan";
		this->button_rescan->Size = System::Drawing::Size(180, 23);
		this->button_rescan->TabIndex = 24;
		this->button_rescan->Text = L"ReScan";
		this->toolTip_Info->SetToolTip(this->button_rescan, L"Tat o sa fie bine\r\nCand tata va veni\r\nPe mama o va fute \r\nPe noi ne va pizdi");
		this->button_rescan->UseVisualStyleBackColor = true;
		this->button_rescan->Click += gcnew System::EventHandler(this, &mainForm::button4_Click);
		// 
		// label4
		// 
		this->label4->AutoSize = true;
		this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->label4->Location = System::Drawing::Point(13, 138);
		this->label4->Name = L"label4";
		this->label4->Size = System::Drawing::Size(45, 16);
		this->label4->TabIndex = 14;
		this->label4->Text = L"Parity:";
		// 
		// label5
		// 
		this->label5->AutoSize = true;
		this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
			static_cast<System::Byte>(204)));
		this->label5->Location = System::Drawing::Point(13, 165);
		this->label5->Name = L"label5";
		this->label5->Size = System::Drawing::Size(60, 16);
		this->label5->TabIndex = 16;
		this->label5->Text = L"Stop bits";
		// 
		// BottomToolStripPanel
		// 
		this->BottomToolStripPanel->Location = System::Drawing::Point(0, 0);
		this->BottomToolStripPanel->Name = L"BottomToolStripPanel";
		this->BottomToolStripPanel->Orientation = System::Windows::Forms::Orientation::Horizontal;
		this->BottomToolStripPanel->RowMargin = System::Windows::Forms::Padding(3, 0, 0, 0);
		this->BottomToolStripPanel->Size = System::Drawing::Size(0, 0);
		// 
		// TopToolStripPanel
		// 
		this->TopToolStripPanel->Location = System::Drawing::Point(0, 0);
		this->TopToolStripPanel->Name = L"TopToolStripPanel";
		this->TopToolStripPanel->Orientation = System::Windows::Forms::Orientation::Horizontal;
		this->TopToolStripPanel->RowMargin = System::Windows::Forms::Padding(3, 0, 0, 0);
		this->TopToolStripPanel->Size = System::Drawing::Size(0, 0);
		// 
		// RightToolStripPanel
		// 
		this->RightToolStripPanel->Location = System::Drawing::Point(0, 0);
		this->RightToolStripPanel->Name = L"RightToolStripPanel";
		this->RightToolStripPanel->Orientation = System::Windows::Forms::Orientation::Horizontal;
		this->RightToolStripPanel->RowMargin = System::Windows::Forms::Padding(3, 0, 0, 0);
		this->RightToolStripPanel->Size = System::Drawing::Size(0, 0);
		// 
		// LeftToolStripPanel
		// 
		this->LeftToolStripPanel->Location = System::Drawing::Point(0, 0);
		this->LeftToolStripPanel->Name = L"LeftToolStripPanel";
		this->LeftToolStripPanel->Orientation = System::Windows::Forms::Orientation::Horizontal;
		this->LeftToolStripPanel->RowMargin = System::Windows::Forms::Padding(3, 0, 0, 0);
		this->LeftToolStripPanel->Size = System::Drawing::Size(0, 0);
		// 
		// ContentPanel
		// 
		this->ContentPanel->Size = System::Drawing::Size(148, 71);
		// 
		// groupBox_hide
		// 
		this->groupBox_hide->CausesValidation = false;
		this->groupBox_hide->Controls->Add(this->comboBox1);
		this->groupBox_hide->Controls->Add(this->label1);
		this->groupBox_hide->Controls->Add(this->comboBox2);
		this->groupBox_hide->Controls->Add(this->label2);
		this->groupBox_hide->Controls->Add(this->comboBox5);
		this->groupBox_hide->Controls->Add(this->label5);
		this->groupBox_hide->Controls->Add(this->label3);
		this->groupBox_hide->Controls->Add(this->comboBox4);
		this->groupBox_hide->Controls->Add(this->comboBox3);
		this->groupBox_hide->Controls->Add(this->label4);
		this->groupBox_hide->Location = System::Drawing::Point(300, 13);
		this->groupBox_hide->Name = L"groupBox_hide";
		this->groupBox_hide->Size = System::Drawing::Size(193, 197);
		this->groupBox_hide->TabIndex = 21;
		this->groupBox_hide->TabStop = false;
		// 
		// label6
		// 
		this->label6->AutoSize = true;
		this->label6->Location = System::Drawing::Point(266, 339);
		this->label6->Name = L"label6";
		this->label6->Size = System::Drawing::Size(35, 13);
		this->label6->TabIndex = 25;
		this->label6->Text = L"label6";
		// 
		// statusStrip1
		// 
		this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
			this->toolStripStatusLabel_comPort,
				this->toolStripStatusLabel_baudRate, this->toolStripProgressBar_comSatus
		});
		this->statusStrip1->Location = System::Drawing::Point(0, 339);
		this->statusStrip1->Name = L"statusStrip1";
		this->statusStrip1->Size = System::Drawing::Size(484, 22);
		this->statusStrip1->SizingGrip = false;
		this->statusStrip1->TabIndex = 26;
		this->statusStrip1->Text = L"statusStrip1";
		// 
		// toolStripStatusLabel_comPort
		// 
		this->toolStripStatusLabel_comPort->Name = L"toolStripStatusLabel_comPort";
		this->toolStripStatusLabel_comPort->Size = System::Drawing::Size(16, 17);
		this->toolStripStatusLabel_comPort->Text = L"   ";
		this->toolStripStatusLabel_comPort->ToolTipText = L"Current COM Port";
		// 
		// toolStripStatusLabel_baudRate
		// 
		this->toolStripStatusLabel_baudRate->Name = L"toolStripStatusLabel_baudRate";
		this->toolStripStatusLabel_baudRate->Size = System::Drawing::Size(0, 17);
		// 
		// toolStripProgressBar_comSatus
		// 
		this->toolStripProgressBar_comSatus->Name = L"toolStripProgressBar_comSatus";
		this->toolStripProgressBar_comSatus->Size = System::Drawing::Size(20, 16);
		// 
		// textBox2
		// 
		this->textBox2->Location = System::Drawing::Point(175, 294);
		this->textBox2->Name = L"textBox2";
		this->textBox2->Size = System::Drawing::Size(100, 20);
		this->textBox2->TabIndex = 28;
		this->textBox2->Text = L"Ri";
		// 
		// label7
		// 
		this->label7->AutoSize = true;
		this->label7->Location = System::Drawing::Point(142, 74);
		this->label7->Name = L"label7";
		this->label7->Size = System::Drawing::Size(53, 13);
		this->label7->TabIndex = 29;
		this->label7->Text = L"Received";
		// 
		// label8
		// 
		this->label8->AutoSize = true;
		this->label8->Location = System::Drawing::Point(145, 103);
		this->label8->Name = L"label8";
		this->label8->Size = System::Drawing::Size(35, 13);
		this->label8->TabIndex = 30;
		this->label8->Text = L"label8";
		// 
		// timer1
		// 
		this->timer1->Enabled = true;
		this->timer1->Interval = 1000;
		this->timer1->Tick += gcnew System::EventHandler(this, &mainForm::timer1_Tick);
		// 
		// mainForm
		// 
		this->AccessibleDescription = L"asa ceva";
		this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
		this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
		this->ClientSize = System::Drawing::Size(484, 361);
		this->Controls->Add(this->label8);
		this->Controls->Add(this->label7);
		this->Controls->Add(this->textBox2);
		this->Controls->Add(this->statusStrip1);
		this->Controls->Add(this->label6);
		this->Controls->Add(this->button_rescan);
		this->Controls->Add(this->button_disconnect);
		this->Controls->Add(this->button_connect);
		this->Controls->Add(this->groupBox_hide);
		this->Controls->Add(this->button3);
		this->Controls->Add(this->button2);
		this->Controls->Add(this->listBox_ContactRecord);
		this->Controls->Add(this->label_ContactRecord);
		this->Controls->Add(this->textBox_cLen);
		this->Controls->Add(this->label_cLen);
		this->Controls->Add(this->textBox_MyUID);
		this->Controls->Add(this->label_myUID);
		this->Controls->Add(this->button1);
		this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
		this->MaximizeBox = false;
		this->Name = L"mainForm";
		this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
		this->Text = L"Nicgrade GUI ";
		this->groupBox_hide->ResumeLayout(false);
		this->groupBox_hide->PerformLayout();
		this->statusStrip1->ResumeLayout(false);
		this->statusStrip1->PerformLayout();
		this->ResumeLayout(false);
		this->PerformLayout();

	}
#pragma endregion
	private: System::Void mainForm_Load(System::Object^  sender, System::EventArgs^  e) {
	}

// metode la obiecte

	private: void fiindComPorts(void)
	{
		// get port names
		array<Object^>^ objectArray = SerialPort::GetPortNames();
		// add string array to comboBox
		comboBox1->Items->AddRange(objectArray);
	
	}



// Resize form
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	bool large = (this->Width > 300)? true:false;

	if (large)
	{
		this->Width = 300;
		this->button3->Text = L">>";
	}
	else
	{
		this->Width = 500;
		this->button3->Text = L"<<";
	}

}




private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	if (serialPort1->IsOpen)
		serialPort1->WriteLine(textBox2->Text);

}

private: System::Void comboBox2_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
	//textBox_notify_baud->Text = comboBox2->Text;
}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
	comboBox1->Items->Clear();
	if (comboBox2->Text =="")
		label6->Text = L"none";
	else
		label6->Text = L"est seva";
	fiindComPorts();
}
		 // Connect button
private: System::Void button_connect_Click(System::Object^  sender, System::EventArgs^  e) {
	
	if (comboBox1->Text == "")
	{
		MessageBox::Show("Selecteaza Portul Corespunzator", "Eroare");
		return;
	}
	if (comboBox2->Text == "")
	{
		MessageBox::Show("Selecteaza Viteza Corespunzatoare", "Eroare");
		return;
	}


	serialPort1->PortName = comboBox1->Text;
	serialPort1->BaudRate = Int32::Parse(comboBox2->Text);
	
	//  Parity select
	if (comboBox4->Text == "none")
		serialPort1->Parity = Parity::None;
	if (comboBox4->Text == "odd")
		serialPort1->Parity = Parity::Odd;
	if (comboBox4->Text == "even")
		serialPort1->Parity = Parity::Even;
	if (comboBox4->Text == "mark")
		serialPort1->Parity = Parity::Mark;
	if (comboBox4->Text == "space")
		serialPort1->Parity = Parity::Space;

	// Data bit select
	if (comboBox3->Text == String::Empty)
	{
		MessageBox::Show("nai selectat data bit ");
	}
	else
	{
		serialPort1->DataBits = Int32::Parse(comboBox3->Text);
	}

	
	serialPort1->Open();

	
	toolStripStatusLabel_comPort->Text = serialPort1->PortName;
	toolStripStatusLabel_baudRate->Text ="|"+ Convert::ToString(serialPort1->BaudRate);
	toolStripProgressBar_comSatus->Value = 100;
	
		
	

	
	//enable ComPort parametres
	groupBox_hide->Enabled = false;
	// disable Connect button 
	button_connect->Enabled = false;
	// enable Disconect button
	button_disconnect->Enabled = true;


}

		 //Disconnect button
private: System::Void button_disconnect_Click(System::Object^  sender, System::EventArgs^  e) {
	//enable ComPort parametres
	groupBox_hide->Enabled = true;
	button_connect->Enabled = true;


	serialPort1->Close();
	toolStripProgressBar_comSatus->Value = 0;
	

}

private: System::Void serialPort1_DataReceived(System::Object^  sender, System::IO::Ports::SerialDataReceivedEventArgs^  e) {
		
	String^ received;

	rxData = serialPort1->ReadLine();
	
		//textBox1->Text = serialPort1->ReadLine();
		//MessageBox::Show(received);
		
		//bel8->Text = L"da";
		
	
	
}



private: System::Void textBox2_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) 
{
	if (e->KeyCode == Keys::Enter)
		//MessageBox::Show("o fost apasat ENTEr");
		if (serialPort1->IsOpen)
		{
			serialPort1->WriteLine(textBox2->Text);
		}

}
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) 
{
	static int a = 0;
	if (serialPort1->IsOpen)
		label8->Text = Convert::ToString(serialPort1->BytesToRead);
	a++;
}
};
}
