#ifndef COIL_H
#define COIL_H

#define coilPin             PD4     // pin 4
//Macros
// #define coilOFF() (PORTD&=~(1<<CoilPin);)		// coilPin=Low
// #define coilON() (PORTD|=(1<<CoilPin);)			// coilPin=High

inline void coilOFF()
{
	PORTD&=~(1<<coilPin);
}

inline void coilON()
{
	PORTD|=(1<<coilPin);
}



#endif //COIL_H