#ifndef EEPROM_H
#define EEPROM_H


#define eepromStartAddr     0x000
#define eepromEndAddr       0x3FF

#define cPTR_high           0x020     //contact pointer address hingh reg
#define cPTR_low            0x021     //contact pointer address low reg



/*  EEPROM function  */
unsigned char eepromRead(unsigned int address)
{
  while (EECR & (1<<EEPE));   //wait for completion of previous write
  EEAR=address;               //set address register
  EECR|=(1<<EERE);
  return EEDR;

}

 unsigned int eepromReadInt(unsigned int address)
{
   return ((eepromRead(address++)<<8)+eepromRead(address));
  
}

void eepromWrite(unsigned int address,
                 unsigned char data)
{
  //wait for completion of previous write
  while (EECR & (1<<EEPE));
  EEAR=address;             //set adress register 2bytes
  EEDR=data;                //set data register
  //write logical on to EEMPE
  EECR|=(1<<EEMPE);
  //start EEPROM write by setting EEPE
  EECR|=(1<<EEPE);

}


void eepromWriteInt( unsigned int address,
                     unsigned int data)
{
  eepromWrite(address++, (data>>8));
  // address++;
  eepromWrite(address, data);
}

void eepromWriteBlock(  unsigned int address,
                        char *block,
                        unsigned int blockSize=1)
{
  for (int i=0; i<blockSize; i++)
  {
    eepromWrite(address,*block);
    address++;
    block++;
  }
}

void eepromErase()
{
  for(unsigned int i=0; i<=eepromEndAddr; i++)
  {
    eepromWrite(i, 0xFF);
  }
}

/* scoate toate valorile (flagurile) din memorie
 * pt var globale predifinite mai sus 
 */







#endif // EEPROM_H
