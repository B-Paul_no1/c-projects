
/*    Modul RFID 522  | Arduino   | CC
  -----------------------------------------
    MISO              | Pin 12    | PB4
    MOSI              | Pin 11    | PB3
     SCK              | Pin 13    | PB5
     SDA (SS)         | Pin 9     | PB1
     RST              | Pin 10    | PB2

  FUSE: LOW:  0xFF,
        HIGH: 0xD7,
        EXT:  0x07.
  F_CPU 16 MHz

  ->Write/Erase Cycles: 10,000 Flash/100,000 EEPROM
*/

#include <SPI.h>
#include <MFRC522.h>

#include "dataPacketID.h"
#include "EEPROM.h"


unsigned int cPTR;          //contact address pointer
// #define cPTR_high           0x020     //contact pointer address hingh reg
// #define cPTR_low            0x021     //contact pointer address low reg


#define RST_PIN             10      // PB2
#define SS_PIN              9       // PB1
      
#define LedOk               PD3     // pin 3
#define BtPin               PC1     // pin A1
#define coilPin             PD4     // pin 4

#define CCSREGF             0x010     // Contact Card Register Flag

#define startContactAdrr    0x0E0
#define myUIDaddr           0x0DC
#define myUidFlagAddr       0x0DB


#define cCT_high            0x000     //contact counter 
#define cCT_low             0x001     //contact counter


#define packetLenght        80        // max data buffer



#define Auth_Key_A  MFRC522::PICC_CMD_MF_AUTH_KEY_A    //0x60    
 
  

byte readOK=0;

byte count=0;

bool serialRead=false;
int cLen = 0;
byte dataBuff[80];
byte in = 0;

byte pLen = 76;     // packet lenght
byte dp = 0;      // data packet necesari

//CC variables:
// char UID[4]={0};
char previousUID[4]={0};
uint8_t cTT;                 //contact counter 

// byte inData[packetLenght]={0};


MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

//Crearea si initializierea cheiei
MFRC522::MIFARE_Key key = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};



inline void LedOFF() { PORTD&=~(1<<LedOk); }     //turn OFF led 

inline void LedON()  { PORTD|=(1<<LedOk);  }     //turn ON led 

inline void coilOFF(){ PORTD&=~(1<<coilPin); }    // disable self chip

inline void coilON() { PORTD|=(1<<coilPin); }




//////////////////////////////////////////////////////////////
inline void serialDataExchange()
{
  serialRead=true;
  UID[0] = eepromRead(0xDC);  // 0xE5
  UID[1] = eepromRead(0xDD);  //
  UID[2] = eepromRead(0xDE);  //
  UID[3] = eepromRead(0xDF);  //

  // read cLen
  cLen = (eepromRead(0x00));
  cLen <<= 8; // MSB
  cLen += (eepromRead(0x01));

  // trimitem datele pe serial
  while(serialRead)
  {
    sendUID();
    delay(50);

    while (1) // wait for response  (atReady)
    {
      UARTreadBlock(dataBuff);
      in = dataPackID(dataBuff);
      flushBuff(dataBuff);
      if (in == ansReady) break;  //exit of loop
      delay(10);
    }

    // send Contact Lenght
    sendByte(SOH);
    sendByte(atContactLen);     // PID
    sendByte(cLen >> 8);        // High byte 
    sendByte(cLen);             // Low byte 
    sendByte(EOT);

    // calc cycles of read data packet
    dp = cLen / 76;
    dp += (cLen % 76) ? 1 : 0;

    // send contacts
    if (dp)
    {
      for (byte i = 0; i < dp; i++)
      {
        //wait atReady from PC
        while (1) // wait for response  (atReady)
        {
          UARTreadBlock(dataBuff);
          in = dataPackID(dataBuff);
          flushBuff(dataBuff);
          if (in == ansReady) break;
          delay(10);
        }

        if (cLen < 76) pLen = cLen % 76;

        if (cLen >= 76)
        {
          pLen = 76;
          cLen -= 76;
        }

        // packet structure
        arr[0] = SOH;
        arr[1] = atContacts;
        arr[2] = pLen;

        // get eeprom contact block

        for (byte i = 0; i < pLen; i++)
        {
          // arr[i+3]=eepromRead(0x0E0+cPTR+i);
          arr[i + 3] = eepromRead(0x0E0 + cPTR);
          cPTR++;
        }

        if (cLen==800) arr[pLen-1]=eepromRead(0x3FF);
       
        arr[pLen + 3] = EOT;
        
        // cPTR=pLen;
        send(arr, pLen + 4);

        delay(10);
      }
    }

    while (1) // wait for response  (atReady)
    {
      UARTreadBlock(dataBuff);
      in = dataPackID(dataBuff);
      flushBuff(dataBuff);
      if (in == ansReady) break;
      delay(10);
    }

    // send atReset
    sendReset(0x04);
    serialRead=false;

  } // end while(serialRead)
}

//////////////////////////////////////////////////////////////

inline void blinkLEDok(void)
{
  PORTD &= ~(1 << LedOk);
  delay(500);
  PORTD |= (1 << LedOk);
  delay(500);
}


// soft Chip Reset
void (*resetFunc)(void)=0;

/********************************  MAIN ****************************/
void setup() 
{
  DDRD|=(1<<LedOk);     //out
  DDRD|=(1<<coilPin);   //out

  Serial.begin(9600); // Initialize serial communications with the PC
  SPI.begin();        // Init SPI bus
  mfrc522.PCD_Init(); // Init MFRC522 card


  if (eepromReadInt(0)==0x00 || eepromReadInt(0)==0xFFFF) // if(cLen==0) then cptr=0xE0;
  {
    cPTR=startContactAdrr;
    eepromWriteInt(0x20,cPTR);
    eepromWriteInt(0x00,0x00); // reset if (cptr==0xFFFF)
  }

}
/***************************** END setup() *****************************/        




/***************************** BEGIN Loop *****************************/        
void loop() 
{
  // daq gasim pe RX atReady data packet
  // if (true)
  if( Serial.read()== SOH &&  Serial.read()== atReady &&
      Serial.read()== EOT &&  Serial.read()== EOT )
  {
    serialDataExchange();
  }

  // rutine function 
  // blinkLEDok();

//------------------------------------------------------------------
  coilON();               //enable MF0U     
 
  // Button Press
  if (PINC &(1<<BtPin))   //verificam daq e apasat butonul
  { 
    LedON();
    readOK=true;          //enable RFID Read Flag
    delay(300);
    LedOFF();     
  }


  // Autificare, optiune obligatorie
  mfrc522.PCD_Authenticate(Auth_Key_A,7,&key,&(mfrc522.uid));             
                        


  // while (true)   // uno
  while (readOK)
  {
    coilOFF();
    LedON();  

    // Uita-te pentru noi carduri
    if ( (! mfrc522.PICC_IsNewCardPresent()) || 
         (! mfrc522.PICC_ReadCardSerial()) )       
      return;
    


    
    cPTR=eepromReadInt(0x20);  

    // if is memory is FULL or overload memory
    
    if(cPTR>0x3FC) 
    {
      // set overload contact lenght flag
      continue;
    } 

    

    Serial.print("next PTR= ");
    Serial.println(cPTR);
    Serial.print("Card UID:");
    dump_byte_array(mfrc522.uid.uidByte, mfrc522.uid.size);  // print uid


    
  

    // copyAtoB(mfrc522.uid.uidByte, UID);
    UID[0]=mfrc522.uid.uidByte[0];
    UID[1]=mfrc522.uid.uidByte[1];
    UID[2]=mfrc522.uid.uidByte[2];
    UID[3]=mfrc522.uid.uidByte[3];
   


   
    //dublicate check 
    if ( 
      previousUID[0]==UID[0] &&
      previousUID[1]==UID[1] &&
      previousUID[2]==UID[2] &&
      previousUID[3]==UID[3] )

    {
      Serial.println(" <-Se repeta blea!");
    }
    else 
    { // increment address pointe
      

      //save new UID in EEPROM 
      eepromWriteBlock(cPTR, UID,4);
      cPTR+=4;
      //save in EEPROM
      eepromWriteInt(0x20,cPTR);            // save contact adres pointe cPTR

      eepromWriteInt(0x00,(cPTR-0xE0));     // contact lenghts bytes

      eepromWrite(0x02, (cPTR-0x0E0)/4);    //contacte inregistrate 
     

    }


    previousUID[0]=UID[0];
    previousUID[1]=UID[1];
    previousUID[2]=UID[2];
    previousUID[3]=UID[3];

    Serial.println();
    
    // Halt PICC
    mfrc522.PICC_HaltA(); // regim de asteptare 
    // Stop encryption on PCD
    mfrc522.PCD_StopCrypto1();
    delay(200);
    readOK=false;          // disable RFID Read flag

 
  } // end if(readOk) 0xFF
LedOFF(); 

}
/***************************** END Loop *****************************/



// void defaulConfig()
// {
//   // eepromErase();
//   // eErase();
//   //reset contact Pointer Address
//   eepromWrite(cPTR_low, startContactAdrr);
//   eepromWrite(cPTR_high, 0x00);
  

//   //reset Contact counter
//   eepromWrite(cCT_low,0x00);
//   eepromWrite(cCT_high,0x00); 
//   // Reset Status Flag
//   eepromWrite(CCSREGF,0x00); 

//   // eepromWriteBlock(0x40,"Manoli Pavel", 12);
//   // eepromWriteBlock(0x50,"NicGrade SRL", 12);

// }



/***************************** BEGIN dump_byte_array() *****************************/        
void dump_byte_array(byte *buffer, byte bufferSize) {
    for (byte i = 0; i < bufferSize; i++) {
        Serial.print(buffer[i] < 0x10 ? " 0" : " ");
        Serial.print(buffer[i], HEX);
        }
}
/***************************** END dump_byte_array() *****************************/        
