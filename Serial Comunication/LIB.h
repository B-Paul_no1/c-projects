/*http://playground.arduino.cc/Interfacing/CPPWindows*/
// Font used Consolas
// Manoli Pavel 2016

#ifndef LIB_H_INCLUDED
#define LIB_H_INCLUDED

#include <iostream>
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>      //pt lucru cu fisierele
#include "Serial.h"     // pt lucru cu comunicarea seriala
//#include "Serial.cpp"


/*  Organizarea containerului de date bData
                  0    1-76  77  78  79
        bData[]={SOH, [...], CR, LF, EOT}
*/
//Macrouri:
#define arrSize(x) (sizeof(x)/sizeof(x[0])) //lungimea vectorului
#define asciiFiltre(x) (x<33)?asciiSpecial[x]:x
//convert int to string
#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()



#define cmd_Grn system("color a")
#define cmd_CLR system("CLS")
#define SOH 0x01				//Start Of Heading
#define SOT 0x02				//Start Of Text
#define ETX 0x03				//End Of Text
#define EOT 0x04				//End Of Transmission
#define ENQ 0x05				//Enquiry (cerere)
#define CR  0x0D				//Carriage Return '\n'
#define LF	0x0A				//Line Feed


using namespace std;
typedef uint8_t byte;

    int contactLen=87;
            // 193  210  243
    int VID[3]={0xC1,0xD2,0xF3};        // Vendor  ID
    int PID[3]={0x03,0x03,0x03};        // Product ID
            // 170  181  196  221
    int UID[4]={0xAA,0xB5,0xC4,0xDD};  //exemplu de numCard

    int contacts[16]={0x01,0x02,0x03,0x04,
                      0x05,0x06,0x07,0x08,
                      0x09,0x0A,0x0B,0x0C,
                      0x0D,0x0E,0x0F,0xAA};


/*
    Comenzi AT, AT(attention)
    folosim intervalul 128...255 (partea a doua a ASCII)
*/
enum  atCommand{AT=   0x80,  //128,     //comanda rpincipala
                atPVD=0x81,  //129      // cere PID si VID
                atUID=0x82,  //131

                atContactLen= 0x83, //nr de octeti inregistrati
                atContacts=   0x84, //extrage vectorul cu contate

                atReady,
                atSOH=0x01
                };

byte bData[80];      //tamponul de date pt transmitere/primire


//convert String to Char*
inline char* strToChar(string str)
{
     char *ch;
     ch=(char*)malloc(str.size()+1);
     memcpy(ch,str.c_str(),str.size()+1);
    return ch;
}



//verifica ce comanda este i bData
char* cmdDetect(byte *arr,  //unde detectam comanda
                int  *ar)   // returnam comanda
{
    if (arr[0]==SOH)
    {
        switch(arr[1])
        {
        case AT:
            return "Vrea AT";
            break;

        case atUID:
        //uid:
            ar[0]=arr[2];
            ar[1]=arr[3];
            ar[2]=arr[4];
            return ">> Prelucrarea UID";
            break;
        case atPVD:
            //pid:
            ar[0]=arr[2];
            ar[1]=arr[3];
            ar[2]=arr[4];
            //vid:
            ar[3]=arr[5];
            ar[4]=arr[6];
            ar[5]=arr[7];
            return ">> Prelucrarea PID si VID";
        case atContactLen:
            *ar=arr[2];   //cLen, contacte inregistrati in octeti
            return ">> Etragerea nr de contacte";
            break;

        case 0:
            return "vreat PID";
            break;

        default:

            return "nu sunt Comenzi";
        }

    }
    else
        return "not OK";

    return "oK";
}

//afisarea matricei in consola
int bDataPrint(byte *bd)
{
	cout<<hex<<uppercase;
    for (byte i=0; i<80; i++)
    {
        if(*bd+0<16)
    	{
    		cout<<"0"<<*bd+0<<" ";
    		// cout<<*bd+0<<" ";
    	}

        else
        {
        	cout<<*bd+0<<" ";
        	// cout<<*bd<<" ";
        }
        if (  i==19
        	||i==39
        	||i==59
        	||i==79
        	) cout<<endl;
        bd++;
         //return i;
    }

    cout<<dec<<endl;
}

//resetarea vector

byte resetVector(byte V[], 				//vectorul de intrare
				 byte lenVec,			//lungimea vectorului de intrare
				 bool autoInsert=false)	// complectarea cu SOH si EOT
{
    while (lenVec)
    {
        V[lenVec]=0;
        lenVec--;

    }
    V[0]=0;
    if (autoInsert)
    {
	    V[0]=SOH;
	    V[79]=EOT;
    }
    return 0;
}


void jsonFormat(ofstream &of, 		//fisierul ude se inscrie
				int *uid,	  		// vector pt UID
				int *Contacts ,     // vector pt contacte
				int nrContacts=0    // Lenght vector pt contacte
                /*contacte inregistrate=nrContacts/4*/
				)
{
    //int PID[3]={0x03,0x03,0x03};

    int *ptr=&Contacts[0];
    // of<<" \n\nfa "<<*(ptr)<<endl;
    of<<"{"<<endl;
    of<<"\t\"CardID\":[ "
                        <<uid[0]<<", "
                        <<uid[1]<<", "
                        <<uid[2]<<", "
                        <<uid[3]<<" ],"<<endl;
    of<<hex<<uppercase<<"\t\"CardIDhex\":[ "
                        <<uid[0]<<", "
                        <<uid[1]<<", "
                        <<uid[2]<<", "
                        <<uid[3]<<" ],"<<dec<<endl;

    of<<"\t\"nrContacts\": "<<(nrContacts/4)<<","<<endl;
    of<<"\t\"Contacts\":"<<endl;
    of<<"\t["<<endl;

    if(!nrContacts) of<<"\t\t{ \"ID\" : "<<"null"<<" }"<<endl;
    while(nrContacts)
    {
    	of<<"\t\t{ \"ID\" : [ "<<*ptr;
    	ptr++;
    	of<<", "<<*ptr;
    	ptr++;
    	of<<", "<<*ptr;
    	ptr++;
    	if(nrContacts==4) of<<", "<<*ptr<<" ]}"<<endl;
    	else of<<", "<<*ptr<<" ]},"<<endl;
    	ptr++;
  	   	nrContacts-=4;
    }
    of<<"\t]"<<endl;
    of<<"}";
}


//asa radi pricola
char asciiSpecial[33][5]={"NULL","SOH","STX","ETX","EOT","ENQ","ACK","BEL",
                  		  "BS","HT","LF","VT","FF","CR","SO","SI",
                 		  "DLE","DC1","DC2","DC3","DC4","NAK","SYN","ETB",
                  		  "CAN","EM","SUB","ESC","FS","GS","RS","US","SP"};

//enum of COM Ports
bool PortIsAvailable(char *portName)
{
    HANDLE hSerialPort;
    bool available=false;
    // COMSTAT comStatus;
    // DWORD errors;

    hSerialPort=CreateFile(portName,                 //nume fisier
                          GENERIC_READ|GENERIC_WRITE,   //accese la fisier
                          0,
                          NULL,
                          OPEN_EXISTING,                //operatii cu fs
                          FILE_ATTRIBUTE_NORMAL,        //atribuire fisier
                          NULL);                       //descriptor fd sablon

    if (hSerialPort==INVALID_HANDLE_VALUE)
        available=false;
    else
        available=true;

    CloseHandle(hSerialPort);

    return available;
}

inline bool availableCOMports(bool showCOM=true)
{
    bool avCom=false;
	for (int i=0; i<16; i++)
    {
        if(PortIsAvailable(cPorts[i]))
        {
          if (showCOM)
           cout<<"COM"<<i<<endl;
          avCom=true;
        }
    }
    return avCom;
}

void listenComPort(byte *arr, bool active)
{
    Serial *SerialPort=new Serial(cPorts[4]);

    cout<<"\n\t<Start packet>"<<endl;

    if (SerialPort->IsConnected())
    {
        char receivedData; //'incomingData' Datele de Intrare

        while (SerialPort->IsConnected())
        {
            //if (SerialPort->readData(&receivedData,1))

            SerialPort->readData(&receivedData,1);
            *arr=receivedData;
            arr++;

            //afisare fara cimpurile nule
            if (receivedData)
            {
                cout<<hex<<(unsigned char)receivedData+0<<" ";
                if (receivedData==EOT)
                {
                    cout<<dec<<"\n\t<sfirsit de packet>\n\n"<<endl;
                    return;
                }
                if(receivedData==LF) cout<<endl;
                Sleep(50);

            }
        }
    }
    cout<<endl;
    CloseHandle(SerialPort);
}
#endif // LIB_H_INCLUDED

//Device signature = 0x1e950f
