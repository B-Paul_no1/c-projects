
#include "dataPacketID.h"
#include "EEPROM.h"

#define myUIDaddr           0x0DC	// UID[0]
#define startContactAdrr    0x0E0

#define LedOk               PD3

byte dataBuff[80];



// byte ok=true;
byte in = 0;
int cLen = 0;
int cPTR = 0;
// byte x;

byte pLen = 76; 		// packet lenght
byte dp = 0;			// data packet necesari

bool serialRead=false;

void serialDataExchange()
{
	serialRead=true;
	UID[0] = eepromRead(0xDC);	// 0xE5
	UID[1] = eepromRead(0xDD);	//
	UID[2] = eepromRead(0xDE);	//
	UID[3] = eepromRead(0xDF);	//

	// read cLen
	cLen = (eepromRead(0x00));
	cLen <<= 8;	// MSB
	cLen += (eepromRead(0x01));
	// 
	// trimitem datele pe serial
	while(serialRead)
	{
		while(0) // wait for response  (atReady)
	    {
	    	UARTreadBlock(dataBuff);
	    	in=dataPackID(dataBuff);
	    	flushBuff(dataBuff);
	    	if(in==ansReady) break;
	    	delay(10);
	    }

	    sendUID();
	    delay(50);

	    while (1) // wait for response  (atReady)
	    {
	      UARTreadBlock(dataBuff);
	      in = dataPackID(dataBuff);
	      flushBuff(dataBuff);
	      if (in == ansReady) break;	//exit of loop
	      delay(10);
	    }

	    // send Contact Lenght
	   
	      sendByte(SOH);
	      sendByte(atContactLen);			// PID
	      sendByte(cLen >> 8);				// High
	      sendByte(cLen);					// Low
	      sendByte(EOT);
	   

	    // calc cycles of read data packet
	    dp = cLen / 76;
	    dp += (cLen % 76) ? 1 : 0;

    	// send contacts
	    if (dp)
	    {
	      for (byte i = 0; i < dp; i++)
	      {
	        //wait atReady from PC
	        while (1) // wait for response  (atReady)
	        {
	          UARTreadBlock(dataBuff);
	          in = dataPackID(dataBuff);
	          flushBuff(dataBuff);
	          if (in == ansReady) break;
	          delay(10);
	        }

	        if (cLen < 76)
	          pLen = cLen % 76;

	        if (cLen >= 76)
	        {
	          pLen = 76;
	          cLen -= 76;
	        }

	        // packet structure
	        arr[0] = SOH;
	        arr[1] = atContacts;
	        arr[2] = pLen;

	        // get eeprom contact block

	        for (byte i = 0; i < pLen; i++)
	        {
	          // arr[i+3]=eepromRead(0x0E0+cPTR+i);
	          arr[i + 3] = eepromRead(0x0E0 + cPTR);
	          cPTR++;

	        }

	        if (cLen==800) arr[pLen-1]=eepromRead(0x3FF);
	        arr[pLen + 3] = EOT;
	        // cPTR=pLen;
	        send(arr, pLen + 4);

	        delay(10);

	      }
	    }


	    while (1) // wait for response  (atReady)
	    {
	      UARTreadBlock(dataBuff);
	      in = dataPackID(dataBuff);
	      flushBuff(dataBuff);
	      if (in == ansReady) break;
	      delay(10);
	    }

	    // send atReset
	    sendReset(0x04);
	    serialRead=false;
		
	}; // end while(serialRead)

}



void setup()
{
	DDRD |= (1 << LedOk); //out
	PORTD &= ~(1 << LedOk);

	Serial.begin(9600);

	// if (!Serial.available()) 
	// {
	// 	serialDataExchange();
	// }

	
}
//
void loop()
{
	 // daq gasim pe RX atReady data packet
  if( Serial.read()== SOH &&  Serial.read()== atReady &&
      Serial.read()== EOT &&  Serial.read()== EOT)
  {
    serialDataExchange();
  }



	// rutine function 
	PORTD &= ~(1 << LedOk);
	delay(500);
	PORTD |= (1 << LedOk);
	delay(500);
  //   // put your main code here, to run repeatedly:

}
//
