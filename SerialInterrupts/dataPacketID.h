#ifndef DATAPACKETID_H
#define DATAPACKETID_H
#include <Arduino.h>

#define SOH 0x01		// Start Of Heanding
#define STX 0x02		// Start Of Text
#define EOT 0x04		// End Of Transmission
#define ETX 0x03		// End Of Text
#define ENQ 0x05		// Enquiry (cerere)
#define CR  0x0D		// Carriage Return '\n'
#define LF	0x0A		// Line Feed

// AT macro
#define AT 				0x80			// 128
#define atPVD			0x81			// 129
#define atUID			0x82			// 130
#define atContactLen 	0x83			// 131
#define atContacts 		0x84			// 132
#define atReady			0x85			// 133
#define atExit			0x86			// 134
#define atReset			0x87			// 135


// Answers - Raspunsuri
#define ansPVD          0x90            // 144
#define ansUID          0x91            // 145
#define ansConLen       0x92            // 146
#define ansContact      0x93            // 147
#define ansReady        0x94            // 148
#define ansExit         0x95            // 149
#define ansReset        0x96            // 150

#define sendByte(x)		Serial.write(x);

int VID[3]={0xC1,0xD2,0xF3};        // Vendor  ID
int PID[3]={0x03,0x03,0x03};        // Product 
int UID[4]={0xF1,0xA2,0xF3,0xF4};

byte arr[80];				  //PTR


byte line[11]={	SOH,atContacts,
				0x00,0x90,0x97,0x98,
				0xA1,0xA2,0xA3,0xA4,
				EOT};



 inline void sendPVD() 				// #001#129#004#004	M1
{
	// <SOH><atPVD><PID[0]:PID[2],VID[0]:VID[2]><EOT>
	// Serial.println("PDVD");
	sendByte(SOH);
	sendByte(atPVD);
	sendByte(PID[0]); sendByte(PID[1]); sendByte(PID[2]);
	sendByte(VID[0]); sendByte(VID[1]); sendByte(VID[2]);
	sendByte(EOT);
}

inline void sendUID()
{
	// <SOH><atUID><UID[0]:UID[3]><EOT>
	sendByte(SOH);
	sendByte(atUID);
	sendByte(UID[0]); sendByte(UID[1]);  
	sendByte(UID[2]); sendByte(UID[3]);
	sendByte(EOT);
}



inline void sendContacts(byte cLen)				//#001#132#004#004	M4
{
	// readEEPROM(contactStartAddr+PTR, )
	// ans: <SOT><atContacts><PTR><Contact[PTR]:Contact[PTR+75]><EOT>
	sendByte(SOH);
	sendByte(atContacts);
	sendByte(0);			//PTR

	if (cLen!=0)
	{				
		for (byte i=0; i<76; i++)
		{
			sendByte(0xAB);
		}
		sendByte(EOT);
		
	}

	else 	// ans Null Packet
	{	
		sendByte(0);
		sendByte(0xFF);
		sendByte(EOT);

	}

}


inline void sendReady() 					// #001#133#004#004	M5
{
	// <SOH><atReady><EOT><EOT>
	sendByte(SOH);
	sendByte(atReady);
	sendByte(EOT);
	sendByte(EOT);
}

// send Reset dataPacket
inline void sendReset(byte rstFlag)
{
	// <SOH><atReset><rstFlag><EOT>
	sendByte(SOH);
	sendByte(atReset);
	sendByte(rstFlag);
	sendByte(EOT);
}



byte dataPackID(byte *inBuff)	
{
	byte pID=0;					// packet ID 
	static byte cLen='$';	
	// byte *outBuff;			// Contact lenghts
	

	//ask PVD PID&VID
	if (	inBuff[0] == SOH && inBuff[1] == atPVD &&
			inBuff[2] == EOT && inBuff[3] == EOT )
		pID=ansPVD;

	//ask UID
	if (	inBuff[0] == SOH && inBuff[1] == atUID &&
			inBuff[2] == EOT && inBuff[3] == EOT)
		pID=ansUID;

	// Ask(cere) ContactLenghts
	if ( 	inBuff[0] == SOH && inBuff[1]== atContactLen &&
		 	inBuff[2] == EOT && inBuff[3]== EOT )	
		pID=ansConLen;

	// ans Contacts
	if (	inBuff[0] == SOH && inBuff[1] == atContacts && 
			inBuff[2] == EOT && inBuff[3] == EOT )
		pID=ansContact;
	
	// ans ready 
	if (
			inBuff[0] == SOH && inBuff[1] == atReady &&
			inBuff[2] == EOT && inBuff[3] == EOT
		)
		pID=ansReady;

	// ans reset
	if (	inBuff[0] == SOH &&	inBuff[1] == atReset && 	
			inBuff[3] == EOT)
		pID=ansReset;


	return pID;
}




void flushBuff(byte *buff)			// clear arr={0}
{
	for(byte i=0; i<80; i++ )
	{
		asm("nop");
		buff[i]='\0'; 
	}
}

void UARTreadBlock(byte *buff)		//UART read Data Block
{
	byte in;
	byte p=0;
	flushBuff(buff);
	in=Serial.read();
	
	while(in !=0xFF)
	{
		buff[p]=in;
		in=Serial.read();
		p++;
	}
}

void printArray(byte *buff)
{
	if(buff[0]==0) return;

	while(*buff!='\0')
	{
		Serial.print((char)*buff);
		*buff=0xFF;
		buff++;

	}

	Serial.println();
	
}

void send(byte *inBuff, byte len=80)
{
	// while(*inBuff!='\0')
	for(byte i=0; i<len;i++)
	{
		sendByte(*inBuff);
		inBuff++;
	}

}




#endif //dataPacketID_H