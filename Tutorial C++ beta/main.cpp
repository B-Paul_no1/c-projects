#include <iostream>
#include <cstdlib>
#include <string>
#include <cstdio>
#include <conio.h>
#include <fstream>      //pt ofstream si ifstream

using namespace std;


int dM(int a){return 2*a;}          //pt functii pointeri
int dN(int b){return b*b;}          //pt functii pointeri


/*  functie pointer
*@brief 
    <type> (*name)(<arg type>)=&<funcName>
    <type> (*name[])(<arg type>)={0} sau {&<funcName>,...}
*/

//int (*ptrFunc)(int)=dM;
int (*ptrFunc)(int)=&dM;

int (*ptrArrFunc[])(int)={0};       //initializierea obligatorie




struct DataPacket{
            int ID=0;
            int* arg=new int[3];    // Vector(mocrou) dinamic
            int ma();               // Metoda (link)  in structura
                }DP;

int DataPacket::ma(){return 100;}   // metoda structurii DataPacket

const int nn=99;

//mostenirea claselor

//Clasa mama:
class A
{
private:
    int varA=0;
    int funcAp(){return 13;}
public:
    A(){};
    bool truA=false;
    int funcA(){return 10;}
};
//clasa fiica:
class B:public A  // mosteneste tot ce e in public
{
private:
    int varB=0;
    int funcB(void){return 6;}
public:
    B(){};
    bool truC=false;
    int funcC(){return 0;}
}M; //<-declaram ca  var globala


// functia sablon:
template <class T>
    T maxim(T x, T y)
    {
        return (x>y)? x:y;
    }

//clasa sablon
const int SIzE=5;               // Sintaxa:
template <class aTip>           // template<class T>
    class matrice               // class [nume_clasa]
    {
    private:
        aTip a[SIzE];
    public:
        matrice();                  // constructor
        aTip &operator[](int i);    // Supraîncărcarea operatorului []
        void inf();
    };
//Implementare constructor clasa generica
                                    // Sintaxa
template <class aTip>               // template<class T>
    matrice <aTip>::matrice()       // [tip_ret]
        {
        register int i;             // variabila  foarte rapida
        for (i=0; i<SIzE; i++)
            a[i]=i;
    }
//Implementare supraincarcare operator []
template <class aTip>
    aTip &matrice<aTip>::operator[](int i)
    {
        if (i<0 || i>SIzE-1)
        {
            cerr<<"Valoarea indice eronata..";
            getch();
            exit(1);
        }
        return a[i];
    }
//Implementare functie clasa generica
                                    // Sintaxa:
template <class aTip>               // template <class T>
    void matrice <aTip>::inf()      //[tip_ret][nume_clasa] <T>::[nume_functie]
    {
        cout<<"Din clasa  Sablon"<<endl;    // corpul functiei
    }


void adauga(int& a)
{
    a+=3;

}

void dataChange(int newValue, int& var);


// C// Comenzi AT  ,AT(attention)
 enum atCommand{ AT=0x01,            //interogare la sustinerea AT comenzilor
                readByte=0x03,      // declare inplicita
                writeByte,
                logIn=nn,
                status=0xFF,
                atcmd_1=56,

                }ATcmd;             //Definirea obiectului de tipul atCommand




           //Definirea obiectului de tipul atCommand
class sd{int m;};
//#include <windows.h>
/////////////////////////////////////////////////////////
int main()
{   
    int arr[10]={0}; //arr[0]:arr[9]=0,
    int *iPtr=new int(56);  //*iPtr=56

    A a; //apel constructor implicit
    A aa(); // apel constructor fara parametrii

    A *pA;
    pA=new A(); // crearea obiectului dinamic  si apel constructor



    cout<<*arr<<endl;       //  <=> cout<<arr[0]<<endl;
    cout<<*(arr+2)<<endl;   //  <=> cout<<arr[2]<<endl;
    cout<<*(arr+n)<<endl;   //  <=> cout<<arr[n]<<endl;



    {
        typedef unsigned __int8 byte;

        byte df;
        df=128;
        cout<<df+0<<endl;

        //apelarea functiilor pointer simplu si prin masiv
        ptrArrFunc[0]=&dM;
        ptrArrFunc[1]=&dN;
        cout<<"ptrFunc(10)= "<<ptrFunc(10)<<endl;
        cout<<"ptrArrFunc[0](8)= "<<ptrArrFunc[0](8)<<endl;
        cout<<"ptrArrFunc[1](99)= "<<ptrArrFunc[1](99)<<endl;


        __int8 sd;
        cout<<"sizeof __int8 = "<<sizeof(byte)<< " bytes"<<endl;

      
    cout<<endl;
    }
    cout<<"ccc "<<__cplusplus <<endl;

    char *nume="myFile.txt";

    FILE *myFile;
   //FILE *tmpFile;
    //tmpFile=tmpfile();
    //tmpnam("dad.tmp");
    //remove("file.txt"); // remove file



    myFile=fopen(nume,"a");
    // myFile=fopen("myFile.txt","a");


   cout<<"File "<<fopen("myFile.txt","a")<<endl;
    //fprintf(myFile, "ibati 1\n");
   // fprintf(myFile, "ibati 2\n");


    //fclose(myFile);




//delay(1000);


   // cout<<"enum "<<typeof()<<endl;

    cout<<"Sizeof ds ="<<sizeof(sd)<<" byte's"<<endl;
    //ATcmd=AT;
        ATcmd=status;
    cout<<"\nATcmd = "<<ATcmd<<endl;

    switch (ATcmd)
    {
    case AT:
        cout<<"AT is OK"; break;
    case status:
        cout<<"Status is OK"; break;
    default:
        cout<<"NIKA bun"; break;
    }

    /*
    string ms="Ibati shii ghini afara!!!";
    matrice<int>intB;
    matrice<double>doubM;
    int i;
    cout<<"\tO fignea de matrice de tip integer"<<endl;
    ;

    for(i=0;i<SIZE;i++)
    {
        intB[i]=i;
       cout<<intB[i]<<endl;
    }
    cout<<endl;
    cout<<"\tO fignea de matrice de tip double"<<endl;

     for(i=0;i<SIZE;i++)
    {
       doubM[i]=i/1.1;
       cout<<doubM[i]<<endl;
    }
    intB.inf();
    doubM.inf();



    cout<<endl;
    */

    /*
    int n;
    bool m;
    B b;

    n=M.funcA();
    cout<<"n= "<<n<<endl;



    cout<<"Tipa program cu alocare dinamica si eliberarea ei"<<endl;

    md=Alb;
    cout<<"\n \\n "<<'\f'+0<<endl;
    cout<<"Strucasiu= "<<Strucasiu<<endl;


    cout<<endl;

   //cout<<"aa "<<0+func()::sd;
    cout<<"String output: "<<ms<<endl;


    printf("\nibati din C fara %c%c\n",'+',169);

    //ms.append(asa);

    int a=7;
    cout<<"\faa= "<<a<<endl;
    adauga(a);


    dataChange(40,a);
    cout<<"\nNNNNNNNNNNNNNNNNN x>y?= "<<maxim(78.1,78.1)<<endl;
    cout<<"DP.s= "<<DP.ma()<<endl;


    int *p=new int(3);
    int *v=new int[3]{1,2,3};
    int **dynamicArray=new int*[3];// alocarea coloanelor

    for (int i=0; i<=3; i++)    //alocarea linilor
    {
        dynamicArray[i]=new int[3];
    }


    cout<<"*p= "<<*p<<endl;

    for(int i=0; i<3; i++)
        {
            cout<<"*v= "<<*v++<<endl;

        }




    int **nv=new int*[2]; // matrice dinamica bidimensionala

    nv[1]=new int[2];
    nv[1][1]=78;


    cout<<"\nNv= "<<nv[1][1]<<endl;




    delete [] nv;
    delete p;
    delete[] v, dynamicArray;

    */
    cout << "\nHello world!\n" << endl;
    //abort();
    system("echo \tEntereaza pt iesire");
   system("pause");

    return 0;
}

void dataChange(int newValue, int& var)
{

    var=newValue;
};
