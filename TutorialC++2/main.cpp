/*//#include <iostream>
//#include <stdlib.h>
//#include <math.h>
//
//
//using namespace std;
//
//namespace rsa
//{
//    class RSA
//    {
//
//        /*proprietati*/
//    private: int prime1,prime2,d;
//    public: int e=7;
//
//
//    public:
//
//        /*  ############## Metode ##############*/
//        //Constructor
//        RSA()
//        {
//            cout<<"Apel constructor RSA"<<endl;
//        }
//
//        //cnstr cu parametrii
//        RSA(int prime1, int prime2, int encryptKey)
//        {
//           this->prime1=prime1;
//           this->prime2=prime2;
//           this->e=encryptKey;
//        }
//
//        //alg Delfie-Hellman
//        int crypt(int mes, int exp, int modul)
//        {
//            long buff=0;
//            buff= pow(mes,exp);
//            buff=buff%modul;
//            return (int)buff;
//        }
//
//
//        int crypt(int mesaj)
//        {
//            crypt(mesaj, e, nValue());
//        }
//
//        int decrypt(int mesaj)
//        {
//            crypt(mesaj, privateKey(),nValue());
//        }
//
//
//        // valoare lui N=prim1*prim2
//        int nValue()
//        {
//
//            return (this->prime1*this->prime2);
//        }
//
//        // phi=prime-1*prime2-1
//        int phi()
//        {
//
//            return ((prime1-1)*(prime2-1));
//        }
//
//        //calc privatekey
//        int privateKey()
//        {
//            d=(phi()+1)/e;
//            return d;
//        }
//
//        void rsaPrintInfo()
//        {
//            cout<<"\nInfo:\n";
//            cout<<"Prime 1= "<<prime1<<endl;
//            cout<<"Prime 2= "<<prime2<<endl;
//            cout<<"nValue = "<<nValue()<<endl;
//            cout<<"phi(n) = "<<phi()<<endl;
//
//
//            cout<<" Public Key{ "<<e<<", "<<nValue()<<" }"<<endl;
//            cout<<" Private Key{ "<<d<<", "<<nValue()<<" }"<<endl;
//
//
//        }
//        //Destructor
//        ~RSA(){}
//    }; // *********************** END class RSA ***********************//
//
//} // *********************** END namespace rsa ***********************//
//
//
//
//using namespace rsa;
//
//// mes^exp mod modul
//int a(int mes, int exp, int modul)
//{
//    long buff=0;
//    buff=pow(mes,exp);
//    buff=buff%modul;
//    return (int)buff;
//}
//
//// *********************** BEGIN main(): ***********************//
//int main()
//{
//    RSA rsa(3,11,7); //rsa(prime1, prime2, encryptKey)
//
//    int e=rsa.e;
//    int n=rsa.nValue();
//    int d=rsa.privateKey();
//
//    int mesaj=0;
//
//
//    rsa.rsaPrintInfo();
//
//    cout<<"Introdu nr pt cryptare 0<= "<<" x < "<<rsa.nValue()<<": \n";
//    cin>>mesaj;
//
//    cout<<"Original: "<<mesaj<<endl;
//    mesaj=rsa.crypt(mesaj);
//
//    cout<<"Cryptat: "<<mesaj<<endl;
//    mesaj=rsa.decrypt(mesaj);
//
//    cout<<"Decryptat "<<mesaj<<endl;
//
//    cout << "\nsizeof(rsa)= " <<sizeof(rsa)<<" bytes" <<endl;
//    /*
//    cout << "a(5,7,33)= "<<a(5,7,33) << endl;
//    cout << "a(5,7,33)= "<<az.crypt(5,7,33) << endl;
//    cout << "az(14,3,33)= "<<az.crypt(14,3,33) << endl;*/
//
//
//
//    return 0;
//
//} // *********************** END main(): ***********************//


#include <iostream>
#include <ctime>
//#include <stdio.h>
#include <cmath>
#include <string.h>
#include <cstdlib>
#include <iomanip>
using namespace std;



/*Sita lui Sundaram, descopera nr prime de la 1 pina la n */
/////////////////////////////////////////////////////////////////////
//Алгоритм "решето Сундарама". Выбирает все простые числа
//до заданного (случайно сгенерированного).

int sundaram(int n)
{
	int *a = new int[n], i, j, k;

	 //a=realloc()

    memset(a, 0, sizeof(int)* n);

	for (i = 1; 3 * i + 1 < n; i++)
	{
		for (j = 1; (k = i + j + 2 * i*j) < n && j <= i; j++)
			a[k] = 1;
	}
	//Выбирает из списка простых чисел ближайшее к заданному.
	for (i = n - 1; i >= 1; i--)
	if (a[i] == 0)
	{
		return (2 * i + 1);
		break;
	}
	delete[] a;
}
/////////////////////////////////////////////////////////////////////
//Алгоритм Евклида. Алгоритм для нахождения наибольшего
//общего делителя двух целых чисел. Используется для проверки
//чисел на взаимопростоту.
int gcd(int a, int b)
    {
        int c;
        while (b)
        {
            c = a % b;
            a = b;
            b = c;
        }
        return abs(a);
    }

// Verificarea daca nr este prim
bool IsPrime(int number)
    {
        if (number<2) return false;
        if (number%2==0) return (number==2);

        int rad=(int)sqrt((double)number);

        for (int i=3; i<=rad;i+=2)
        {
            if (number%i==0) return false;
        }
        return true;

    }
/*
lgoritmului lui Euclid, care rezolva ecuatie de forma A * X + B * Y = D,
 unde D este cel mai mare divizor comun al lui A si B.
*/
int euclidAlg(int a, int b)
{

    int c;
    while(b)
    {

        c=a%b;
        a=b;
        b=c;
    }
    return a;
}
/////////////////////////////////////////////////////////////////////

/*Fie „a” şi „n” , două numere naturale, cu n ≥ 2.Produsul a „n” factori egali cu „a” se numeşte puterea a n-a a numărului „a” şi se notează :
Se scrie:      REGULI DE CALCUL CU PUTERI
Se citeşte: „ a la puterea n”.
„ a” se numeşte bază.
„n” se numeşte exponent*/
int crypt(int msg,int exp, int modul )
    {
        unsigned int iter=0,c=1;
        while (iter<exp)
        {
            c = c*msg;
            c = c%modul;
            iter++;
        }
        return c;
    }


int main()
{


    cout << "ASCII encrypt"<<endl;
	cout <<" ----\torig--- "<< "encrypt"<<" ---decrypt"<<endl;
	//cout <<"it:"<<""<<endl;
    for (int i=65;i<=122;i++)
    {
        cout <<"it: "<<i<<" - "<<(char)i<<
        "   -   "<<crypt(i,133,689)<<
        "   -   "<<(char)crypt(crypt(i,133,689),61,689)
        <<endl;

    }






	cout << "Please wait... Key generation procces." << endl << endl;
	// Генерация двух чисел и выбор двух простых чисел.
	srand((unsigned)time(NULL));

	int p = rand() % 100;
	int q = rand() % 100;

	int p_simple = sundaram(p);
	int q_simple = sundaram(q);

	//Находим число n.
	unsigned int n = p_simple*q_simple;
	//Генерация числа d и проверка его на взаимопростоту
	//с числом ((p_simple-1)*(q_simple-1)).
	int d, d_simple = 0;
	while (d_simple != 1)
	{
		d = rand() % 100;
		d_simple = gcd(d, ((p_simple - 1)*(q_simple - 1)));
	}

	//Определение числа e, для которого является истинным
	//соотношение (e*d)%((p_simple-1)*(q_simple-1))=1.
	unsigned int e = 0, e_simple = 0;
	while (e_simple != 1)
	{
		e += 1;
		e_simple = (e*d) % ((p_simple - 1)*(q_simple - 1));
	}

	//Сгенерированные ключи.
	cout << "Cheia Publica{ "<< e << ',' << n << '}' <<endl;
	cout << "Cheia Privata{ "<< d << ',' << n << '}' << endl << endl;

	int ns;
	cout<<"introdu un nr prim"<<endl;
	cin>>ns;
	cout<<"Nr introdus este prim? = ";
	//if versiune prescurtata
	IsPrime(ns) ? cout<< "adevarat\n" : cout<<"fals"<<endl;


	//Ввод шифруемых данных.
	int MAX=20;
	char *Text = new char[MAX];



	cout << "Please enter the Text. Use <Enter> button when done." << endl;
	//cout << "static_cast<int>(a) " <<static_cast<int>('A')<< endl;


	cin.get(Text,MAX);

	//Массив для хранения шифротекста.
	unsigned int *CryptoText = new unsigned int[MAX];
	unsigned int *Tdecrypt = new unsigned int[MAX];
        *CryptoText=(unsigned int)calloc(MAX,sizeof(int));
        *Tdecrypt=(unsigned int)calloc(MAX,sizeof(int));


	//Получение из введённых данных десятичного кода ASCII и
	//дальнейшее его преобразование по формуле ci = (mj^e)%n.
	int b = 301;
	int c;
	cout << endl << setw(5) << "Text" << setw(6) << "ASCII"
		<< setw(20) << "CryptoText/Block#" << setw(14)
		<< "ASCIIdecrypt" << setw(14) << "Text decrypt" << endl
		<< "------------------------------------------------------------" << endl;
	//encrypt Array
	for (int j = 0; j < MAX; j++)
	{
		c = 1;
		unsigned int i = 0;
		int ASCIIcode = (int)Text[j] + b;

		CryptoText[j] = crypt(ASCIIcode,e,n);

		b += 1;
	}
	//Расшифровка полученного кода по формуле mi = (ci^d)%n
	//и перевод его в десятичный код ASCII.
	b = 301;
	int m;
	for (int j = 0; j < MAX; j++)
	{
		m = 1;
		unsigned int i = 0;
		while (i<d)
		{
			m = m*CryptoText[j];
			m = m%n;
			i++;
		}
		m = m - b;
		Tdecrypt[j] = m;
		b += 1;
	}
	for (int j = 0; j < MAX; j++)
	{
		cout << setw(5) << Text[j] << setw(6) << static_cast<int>(Text[j]) << setw(20)
			<< CryptoText[j] << setw(14) << Tdecrypt[j] << setw(14) << static_cast<char>(Tdecrypt[j]) << endl;
	}
	delete[] Text;
	delete[] CryptoText;
	delete[] Tdecrypt;



	//
	cout << "Press ENTER to EXIT\n";
	cout << "Public Key {133,689}\n";
	cout << "Privat Key {61,689}\n"<<endl;
	cout << "Crypt    5 "<<crypt(5,133,689)<<endl;
	cout << "Dcrypt 564 "<<crypt(564,61,689)<<endl;
	cout << "\nPress ENTER to EXIT\n";
	system("pause>>void");
	return 0;
}
//
////int _tmain(int argc, _TCHAR* argv[])
//{
//	return 0;
//}

