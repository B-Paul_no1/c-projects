/*http://www.cyberforum.ru/cpp-builder/thread751062.html*/



HANDLE readerRMT69L;    //дескриптор потока чтения из порта
DWORD WINAPI ReadThreadRMT69L(LPVOID);
void ReadPrintingRMT69L(void);
//---------------------------------------------------------------------------
//главная функция потока, реализует приём байтов из COM-порта
DWORD WINAPI ReadThreadRMT69L(LPVOID)
{
        COMSTAT comstat;        //структура текущего состояния порта, в данной программе используется для определения количества принятых в порт байтов
        DWORD btr, temp, mask, signal;  //переменная temp используется в качестве заглушки
        int T;
 
        overlappedRMT69L.hEvent = CreateEvent(NULL, true, true, NULL);  //создать сигнальный объект-событие для асинхронных операций
        SetCommMask(COMportRMT69L, EV_RXCHAR);                              //установить маску на срабатывание по событию приёма байта в порт
        while(1)                        //пока поток не будет прерван, выполняем цикл
        {
                WaitCommEvent(COMportRMT69L, &mask, &overlappedRMT69L);                 //ожидать события приёма байта (это и есть перекрываемая операция)
                signal = WaitForSingleObject(overlappedRMT69L.hEvent, INFINITE);    //приостановить поток до прихода байта
                if(signal == WAIT_OBJECT_0)                     //если событие прихода байта произошло
                {
                        if(GetOverlappedResult(COMportRMT69L, &overlappedRMT69L, &temp, true)) //проверяем, успешно ли завершилась перекрываемая операция WaitCommEvent
                        if((mask & EV_RXCHAR)!=0)                   //если произошло именно событие прихода байта
                        {
                                ClearCommError(COMportRMT69L, &temp, &comstat);     //нужно заполнить структуру COMSTAT
                                btr = comstat.cbInQue;                              //и получить из неё количество принятых байтов
                                if(btr==59)                                     //если действительно есть байты для чтения
                                {
                                        ReadFile(COMportRMT69L, bufrdRMT69L, btr, &temp, &overlappedRMT69L);     //прочитать байты из порта в буфер программы
                                        ReadPrintingRMT69L();                           //вызываем функцию для вывода данных на экран и в файл
                                        Form1->Label12->Caption="ОК";
                                        Form1->Label12->Font->Color=clGreen;
                                        T=0;
                                }
                                else T++;
                                if (T>3) {Form1->Label12->Caption="ОШИБКА";Form1->Label12->Font->Color=clRed; if (Form1->Image5->Visible==false) Beep(1000,200);}
                        }
                }
        }
}


void COMOpenRMT69L()
{
        String portname;         //имя порта (например, "COM1", "COM2" и т.д.)
        DCB dcb;                //структура для общей инициализации порта DCB
        COMMTIMEOUTS timeouts;  //структура для установки таймаутов
        portname = "\\\\.\\COM12";  //получить имя выбранного порта
        //открыть порт, для асинхронных операций обязательно нужно указать флаг FILE_FLAG_OVERLAPPED
        COMportRMT69L = CreateFile(portname.c_str(),GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
        //здесь:
        // - portname.c_str() - имя порта в качестве имени файла, c_str() преобразует строку типа String в строку в виде массива типа char, иначе функция не примет
        // - GENERIC_READ | GENERIC_WRITE - доступ к порту на чтение/записть
        // - 0 - порт не может быть общедоступным (shared)
        // - NULL - дескриптор порта не наследуется, используется дескриптор безопасности по умолчанию
        // - OPEN_EXISTING - порт должен открываться как уже существующий файл
        // - FILE_FLAG_OVERLAPPED - этот флаг указывает на использование асинхронных операций
        // - NULL - указатель на файл шаблона не используется при работе с портами
        if(COMportRMT69L == INVALID_HANDLE_VALUE)            //если ошибка открытия порта
        {
                // "Не удалось открыть порт";       //вывести сообщение в строке состояния
               
        }
        //инициализация порта
        dcb.DCBlength = sizeof(DCB);    
        /*в первое поле структуры DCB необходимо занести её длину, она будет
         использоваться функциями 
        настройки порта для контроля корректности структуры
       */
        //считать структуру DCB из порта
        if(!GetCommState(COMportRMT69L, &dcb))  //если не удалось - закрыть порт и вывести сообщение об ошибке в строке состояния
        {
                COMCloseRMT69L();
                // Form1->StatusBar1->Panels->Items[0]->Text  = "Не удалось считать DCB";
                return;
        }
        //инициализация структуры DCB
        dcb.BaudRate = 57600;                     //задаём скорость передачи 115200 бод
        dcb.fBinary = TRUE;                       //включаем двоичный режим обмена
        dcb.fOutxCtsFlow = FALSE;                 //выключаем режим слежения за сигналом CTS
        dcb.fOutxDsrFlow = FALSE;                 //выключаем режим слежения за сигналом DSR
        dcb.fDtrControl = DTR_CONTROL_DISABLE;    //отключаем использование линии DTR
        dcb.fDsrSensitivity = FALSE;              //отключаем восприимчивость драйвера к состоянию линии DSR
        dcb.fNull = FALSE;                        //разрешить приём нулевых байтов
        dcb.fRtsControl = RTS_CONTROL_DISABLE;    //отключаем использование линии RTS
        dcb.fAbortOnError = FALSE;                //отключаем остановку всех операций чтения/записи при ошибке
        dcb.ByteSize = 8;                         //задаём 8 бит в байте
        dcb.Parity = 0;                           //отключаем проверку чётности
        dcb.StopBits = 0;                         //задаём один стоп-бит
        //загрузить структуру DCB в порт
        if(!SetCommState(COMportRMT69L, &dcb))  //если не удалось - закрыть порт и вывести сообщение об ошибке в строке состояния
        {
                COMCloseRMT69L();
                // Form1->StatusBar1->Panels->Items[0]->Text  = "Не удалось установить DCB";
                return;
        }  
        //установить таймауты
        timeouts.ReadIntervalTimeout = 0;          //таймаут между двумя символами
        timeouts.ReadTotalTimeoutMultiplier = 0;       //общий таймаут операции чтения
        timeouts.ReadTotalTimeoutConstant = 0;         //константа для общего таймаута операции чтения
        timeouts.WriteTotalTimeoutMultiplier = 0;      //общий таймаут операции записи
        timeouts.WriteTotalTimeoutConstant = 0;        //константа для общего таймаута операции записи
        //записать структуру таймаутов в порт
        if(!SetCommTimeouts(COMportRMT69L, &timeouts))  //если не удалось - закрыть порт и вывести сообщение об ошибке в строке состояния
        {
                COMCloseRMT69L();
                //Form1->StatusBar1->Panels->Items[0]->Text  = "Не удалось установить тайм-ауты";
                ShowMessage("Не удалось установить тайм-ауты RMT69L");
                return;
        }
        //установить размеры очередей приёма и передачи
        SetupComm(COMportRMT69L,2000,2000);
 
        //директории
        TDateTime CurrentDate = Date(); // это текущая дата
        unsigned short year,month,day;
        CurrentDate.DecodeDate(&year, &month, &day);
        TVarRec DateRMT69L[]={day,month,year};
        AnsiString
        path1RMT69L="ТрендыRMT69L\\",
        path2RMT69L="ТрендыRMT69L\\"+Format("%.2d.%.2d.%.2d.txt",DateRMT69L,2);
        //проверка существовиния директорий
        if(DirectoryExists(path1RMT69L.c_str())==false){CreateDirectory (path1RMT69L.c_str(), NULL);}
        //открыть файл или создать если отсутствует для записи данных
        FILE *RMT69L = fopen(path2RMT69L.c_str(),"a+t");
        
        PurgeComm(COMportRMT69L, PURGE_RXCLEAR);     //очистить принимающий буфер порта
        readerRMT69L = CreateThread(NULL, 0, ReadThreadRMT69L, NULL, 0, NULL);          //создаём поток чтения, который сразу начнёт выполняться (предпоследний параметр = 0)
        writerRMT69L = CreateThread(NULL, 0, WriteThreadRMT69L, NULL, CREATE_SUSPENDED, NULL);  //создаём поток записи в остановленном состоянии (предпоследний параметр = CREATE_SUSPENDED)
}