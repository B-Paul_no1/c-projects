
//поток для чтения последовательности байтов из COM-порта в буфер
/*http://www.programmersforum.ru/showthread.php?t=178043*/
class TReadThread : public TThread
{
 private:
       class TComPort *FOwner;
       byte FByte;
       COMSTAT FPState;
       DWORD FMState;
       DWORD FErrCode;
        void __fastcall DoReadByte();
 protected:
        void __fastcall Execute();	//основная функция потока
 public:
        __fastcall TReadThread(TComPort *AOwner);	//конструктор потока
};

typedef void (__closure *TReadByteEvent)(const byte B,COMSTAT PortStat,DWORD ErrCode,DWORD ModemState);


class TComPort
{

private:
    int FComNumber;                        //
    bool GetConnected();                   //
    void SetConnected(const bool Value);   //
    void SetComNumber(const int Value);    //
    void SetBaudRate(const int Value);     //
    void SetByteSize(const int Value);     //
    void SetParity(const int Value);       //
    void SetStopbits(const int Value);     //
    void SetReadActive(const bool Value);  //
    bool GetReadActive();
protected:
    int FBaudRate;   // cкорость обмена (бод)
    int FByteSize;   //число бит в байте
    int FParity;     //четность
    int FStopbits;   // число стоп-бит
    TReadThread *FReadThread;  //читающий поток
    void  DoOpenPort(); //открытие порта
    void  DoClosePort(); //закрытие порта}
    void  ApplyComSettings(); //установка параметров порта}
public:
    HANDLE FHandle;  //дескриптор порта
    TReadByteEvent FOnReadByte; //событие "получение байта"
        //конструктор
        __fastcall TComPort();
        //деструктор
        __fastcall ~TComPort();

        //Открывает/закрывает порт
        void Open();
        void Close();

        //Возвращает True, если порт открыт}
        bool  Connect();
        bool WriteByte(const byte B);

    //Возвращает структуру состояния порта ComStat, а в
    //переменной CodeError возвращается текущий код ошибки
    COMSTAT GetState(DWORD CodeError);
    //Возвращает состояние модема}
    DWORD GetModemState();

        //Возвращает True, если порт открыт
        __property bool Connected = {read = GetConnected, write = SetConnected };
        //Номер порта. При изменении порт переоткрывается, если был открыт}
        __property int ComNumber = {read = FComNumber,write = SetComNumber };
        //Скорость обмена}
        __property int BaudRate = {read = FBaudRate, write = SetBaudRate};
        //Число бит в байте}
        __property int ByteSize = {read = FByteSize, write = SetByteSize};
        //Четность}
        __property int Parity = {read = FParity, write = SetParity};
        //Число стоп-бит}
        __property int Stopbits = { read = FStopbits, write = SetStopbits};
        //Активность чтения порта}
        __property bool ReadActive = {read = GetReadActive, write = SetReadActive};
        //Событие, вызываемое при получении байта}
        __property TReadByteEvent OnReadByte =  {read = FOnReadByte, write = FOnReadByte};

};

__fastcall TComPort::TComPort()
{
FHandle = INVALID_HANDLE_VALUE;
FReadThread = NULL;
}

__fastcall TComPort::~TComPort()
{
DoClosePort();
}

void TComPort::Open()
{
 DoOpenPort();
}

void TComPort::Close()
{
 DoClosePort();
}


//открытие порта}
void TComPort::DoOpenPort()
{
char ComName[10];

 if (Connected) return;
 //Для портов 1..9 можно использовать простые имена COM1..COM9,}
 //но для портов 10-256 надо писать полное имя. Для общности   }
 //будем всегда использовать полное имя порта                  }
 sprintf(ComName,"\\\\.\\COM%d",FComNumber);

 //Открытие последовательного порта
 FHandle = CreateFile(
            ComName, //передаем имя открываемого порта}
            GENERIC_READ | GENERIC_WRITE, //ресурс для чтения и записи}
            0, // не разделяемый ресурс }
            NULL, // Нет атрибутов защиты }
            OPEN_EXISTING, //вернуть ошибку, если ресурс не существует}
            FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, //асинхронный режим доступа}
            0 // Должно быть 0 для COM портов }
           ) ;
 //Если ошибка открытия порта - выход
 if (!Connected) return;
 //Инициализация порта}
 ApplyComSettings();

 FReadThread = new TReadThread(this);	//создать и запустить поток чтения байтов
 FReadThread->Resume();
}

/закрытие порта}
void TComPort::DoClosePort()
{
 if (!Connected) return;
 //Замораживаем поток чтения}
 ReadActive = false;
 //Уничтожение читающего потока
 FReadThread->FreeOnTerminate = true;
 FReadThread->Terminate();
 FReadThread = NULL;
 //Освобождение дескриптора порта}
 CloseHandle(FHandle);
 //Сброс дескриптора порта}
 FHandle = INVALID_HANDLE_VALUE;
}



bool TComPort::WriteByte(const byte B)
{
        DWORD Signaled, RealWrite, BytesTrans;
        OVERLAPPED WriteOL; //структура для асинхронной записи

        bool ret = false;

        //создание события для асинхронной записи}
        memset(&WriteOL,0,sizeof(WriteOL));
        WriteOL.hEvent = CreateEvent(NULL, true, true, NULL);

        try
        {
          //начало асинхронной записи}
          WriteFile(FHandle, &B, 1, &RealWrite, &WriteOL);
          //ожидания завершения асинхронной операции}
          Signaled = WaitForSingleObject(WriteOL.hEvent, INFINITE);
          //получение результата асинхронной операции}
          ret = ((Signaled == WAIT_OBJECT_0) &&
          (GetOverlappedResult(FHandle, &WriteOL, &BytesTrans, False)));
        }
        __finally
        {

          //освобождение дескриптора события}
          CloseHandle(WriteOL.hEvent);
        }

        return ret;
}



//конструктор потока ReadThread, по умолчанию пустой
__fastcall TReadThread::TReadThread(class TComPort *AOwner) : TThread(true)
{
  FOwner = AOwner;
}

//главная функция потока, реализует приём байтов из COM-порта
void __fastcall TReadThread::Execute()
{

    byte B;
    COMSTAT CurrentState;
    DWORD AvaibleBytes, ErrCode, ModemState, RealRead;
    OVERLAPPED ReadOL;
    DWORD Signaled, Mask;
    DWORD BytesTrans;


try{
    memset(&ReadOL,0,sizeof(ReadOL));
    ReadOL.hEvent = CreateEvent(NULL, true, true, NULL);

    SetCommMask(FOwner->FHandle, EV_RXCHAR);

    while ((!Terminated) && FOwner->Connected)
    {
     WaitCommEvent(FOwner->FHandle, &Mask, &ReadOL);
     Signaled = WaitForSingleObject(ReadOL.hEvent, INFINITE);
     if (Signaled  == WAIT_OBJECT_0)
      if (GetOverlappedResult(FOwner->FHandle, &ReadOL, &BytesTrans, false))
       if ((Mask & EV_RXCHAR) != 0)
       {

        CurrentState = FOwner->GetState(ErrCode);
        ModemState   = FOwner->GetModemState();

        AvaibleBytes = CurrentState.cbInQue;

        if (AvaibleBytes > 0)
         if (ReadFile(FOwner->FHandle, &B, 1, &RealRead, &ReadOL))
         {
          FByte   = B;
          FPState = CurrentState;
          FMState = ModemState;
          FErrCode= ErrCode;
          Synchronize(DoReadByte);

         }
       }
    }
}

  __finally {
   CloseHandle(ReadOL.hEvent);
   SetCommMask(FOwner->FHandle, 0);
   } 
}

//выводим принятые байты на экран и в файл (если включено)
void __fastcall TReadThread::DoReadByte()
{
 if (FOwner->OnReadByte) FOwner->FOnReadByte(FByte, FPState, FErrCode, FMState);
}



